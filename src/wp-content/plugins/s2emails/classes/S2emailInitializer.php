<?php
if(!defined('WPINC')) // MUST have WordPress.
    exit("Do not access this file directly.");

if (!class_exists('S2emailInitializer')) {
    /**
    * Initializer class
    */
    class S2emailInitializer
    {
        private static $s2emails_tpls;
        private static $s2emails_sent;
        private static $s2emails_pending_mails;
        private $s2emails_repo;

        public function __construct() {
            global $wpdb;
            
            self::$s2emails_tpls = $wpdb->prefix . S2emailCore::EMAILS_TABLE;
            self::$s2emails_sent = $wpdb->prefix . S2emailCore::SENT_EMAILS_TABLE;
            self::$s2emails_pending_mails = $wpdb->prefix . S2emailCore::PENDING_EMAILS_TABLE;
        }

        /**
         * Init for activation/deactivation/uninstall
         */
        public function init()
        {
            register_activation_hook( MAIN_PLUGIN_FILE, array( $this, 'activation' ) );
            register_deactivation_hook( MAIN_PLUGIN_FILE, array( $this, 'deactivation' ));
            register_uninstall_hook( MAIN_PLUGIN_FILE, array( 'S2emailInitializer', 'uninstall' ));

            // shortcode
            add_shortcode('s2email_shortcode', array($this, 'capture_visiting_page'));

            // admin fires
            add_action('admin_enqueue_scripts', array($this, 'addStyles'));
            add_action('admin_menu', array( $this, 'addMenu' ));
            
            add_action('admin_init', array( $this, 'CheckUpdates' ), 20);
        }

        public function capture_visiting_page($params = array()) {
            // if user is not logged in or is admin, just ignore it
            if (!is_user_logged_in() || is_admin()) {
                return false;
            }
            
            // extract info
            extract($params);

            $current_user     = wp_get_current_user();
            $repo             = $this->getS2emailsRepo();
            $mail_type         = 'shortcode';
            // check if we have already persisted this shortcode pending mail
            $repo->getUserPendingMails($current_user->ID, null, array('"shortcode"'), date('Y-m-d'), '%Y-%m-%d', (int)$id_mail);
            
            if ((int)$repo->get_num_rows) {
                return false;
            }

            // check if the template id is a shortcode one
            $getTemplate = $repo->getTemplateMailById((int)$id_mail);
            if ($getTemplate && $getTemplate->type_of_schedule != 4) {
                return false;
            }

            // add pending mail for this shortcode
            $repo->persistSinglePendingMail($current_user->ID, $getTemplate, $mail_type);
        }

        public static function activation()
        {
            global $wpdb;

            $sql = "CREATE TABLE IF NOT EXISTS ". self::$s2emails_tpls ." (
                `id` INTEGER NOT NULL AUTO_INCREMENT,
                `title` VARCHAR(255),
                `subject` VARCHAR(255),
                `cc_emails` TEXT NULL DEFAULT NULL,
                `time_type` INT(1) default 1 NOT NULL,
                `type_of_schedule` INT(1) default 1 NOT NULL,
                `days` INT(4),
                `hours` INT(2),
                `days_2` INT(4),
                `hours_2` TIME,
                `content` TEXT,
                `status` INT default 1 NOT NULL,
                `content_type` INT default 1 NOT NULL,
                `counter` INT default 0 NOT NULL,
                `roles` TEXT,
                `new_roles` TEXT,
                `active` INT default 1 NOT NULL,
                `create_at` DATETIME DEFAULT NULL,
                `update_at` DATETIME DEFAULT NULL,
                PRIMARY KEY id (id)
            ) ENGINE=InnoDB DEFAULT CHARSET=UTF8";
            $wpdb->query($sql);
        
            $sql = "CREATE TABLE IF NOT EXISTS ". self::$s2emails_sent ." (
                `id` INTEGER NOT NULL AUTO_INCREMENT,
                `user_id` INTEGER(11),
                `mail_id` INTEGER(11),
                `expire_time` VARCHAR(255),
                `sent_time` DATETIME DEFAULT NULL,
                `email` VARCHAR(255),
                `title` VARCHAR(255),
                `subject` VARCHAR(255),
                `content` TEXT,
                `status` INT default 1 NOT NULL,
                PRIMARY KEY id (id)
            ) ENGINE=InnoDB DEFAULT CHARSET=UTF8";
            $wpdb->query($sql);

            // create pending mail table
            $sql = "CREATE TABLE IF NOT EXISTS ". self::$s2emails_pending_mails ."(
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `id_user` int(11) NOT NULL,
                  `id_mail` int(11) NOT NULL,
                  `user_eot` varchar(50) DEFAULT NULL,
                  `send_mail_at` datetime NOT NULL,
                  `mail_type` enum('after_reg','before_exp','after_exp','shortcode','role_update','after_cancel') NOT NULL,
                  `status` tinyint(2) DEFAULT '0',
                  `created_at` datetime DEFAULT NULL,
                PRIMARY KEY `id` (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=UTF8";
            $wpdb->query($sql);

            if (!get_option( 's2emails_from_email')) {
                add_option( 's2emails_from_email', get_option( 'admin_email' )); 
            }

            if (!get_option( 's2emails_from_title')) {
                add_option( 's2emails_from_title', get_option( 'blogname' ));
            }

            add_option( 's2emails_timezone', get_option( 'timezone_string' ));
            add_option( 's2emails_send_auto_email_to', 1);
        }

        public static function deactivation()
        {
            wp_clear_scheduled_hook( 'call_for_mail' );
        }

        public static function uninstall()
        {
            global $wpdb;

            wp_clear_scheduled_hook( 'call_for_mail' );
            delete_option( 's2emails_from_email' );
            delete_option( 's2emails_from_title' );
            delete_option( 's2emails_copy_email' );
            delete_option( 's2emails_cron_job_duration' );
            delete_option( 's2emails_timezone' );
            delete_option( 's2emails_send_auto_email_to' );

            // drop tables
            $sql = "DROP TABLE ". self::$s2emails_tpls .", 
                ". self::$s2emails_pending_mails .",
                 ". self::$s2emails_sent;
            $wpdb->query($sql);
        }

        public function addMenu() 
        {
            $image              = REL_PLUGIN_PATH .'assets/img/icon.png';
            $main_menu_name = S2emailCore::S2EMAILS_MENU_NAME;
            $plugin_name     = S2emailCore::S2EMAILS_PLG_NAME;
            $plugin_cap      = S2emailCore::S2EMAILS_CAP;
            $plugin_slug     = S2emailCore::S2EMAILS_PLG_SLUG;

            add_menu_page( $main_menu_name, $main_menu_name, $plugin_cap, $plugin_name, '', $image );
            add_submenu_page( $plugin_name, 'Automated Emails', 'Automated Emails', $plugin_cap, $plugin_name, array( 'S2emailTemplateManager', 's2emailsManipulateListOfAutomatedEmails' ));
            add_submenu_page( $plugin_name, 'Add New', 'Add New', $plugin_cap, $plugin_name .'-new-automated-email', array( 'S2emailTemplateManager', 'addEditAutomatedEmail' ));
            add_submenu_page( $plugin_name, 'Pending Emails', 'Pending Emails', $plugin_cap, $plugin_name .'-list-pending-emails', array( 'S2emailTemplateManager', 's2emailPeningList' ));
            add_submenu_page( $plugin_name, 'Sent Emails', 'Sent Emails', $plugin_cap, $plugin_name .'-list-sent-emails', array( 'S2emailTemplateManager', 's2emailSentList' ));
            add_submenu_page( $plugin_name, '', '<span style="display:block; margin:1px 0 1px -5px; padding:0; height:1px; line-height:1px; background:#CCCCCC;"></span>', 'create_users', '#');
            add_submenu_page( $plugin_name, 'General Options', 'General Options', $plugin_cap,$plugin_name .'-general-options', array( 'S2emailTemplateManager', 's2emailsGeneralOtptions' ));
            add_submenu_page( $plugin_name, '', '<span style="display:block; margin:1px 0 1px -5px; padding:0; height:1px; line-height:1px; background:#CCCCCC;"></span>', 'create_users', '#');
            add_submenu_page( $plugin_name, 'Getting Started', 'Getting Started', 'manage_options',$plugin_name. '-getting-started', array( 'S2emailTemplateManager', 's2emailsGettingStarted' ));
        }

        /**
         * Set assets
         */
        public function addStyles() {
            wp_enqueue_style( S2emailCore::S2EMAILS_PLG_NAME, REL_PLUGIN_PATH . 'assets/css/style.css', array(), S2emailCore::S2EMAILS_VERSION, 'all' );
        }

        /**
         * Set the repo class
         */
        public function setS2emailsRepo(S2emailRepository $s2emails_repo)
        {
            $this->s2emails_repo = $s2emails_repo;

            return $this;
        }

        /**
         * Get the repo class
         */
        public function getS2emailsRepo()
        {
            return $this->s2emails_repo;
        }

        /**
         * Check for available updates
         */
        public function CheckUpdates()
        {
        	if ( class_exists('S2pluginChecker') ) {
	            $s2pluginchecker = new S2pluginChecker(S2emailCore::S2EMAILS_VERSION, PLUGIN_PATH .'upgrade', 's2emails');
	            $s2pluginchecker->runUpdater();
        	}
        }
    }
}