<?php
if(!defined('WPINC')) // MUST have WordPress.
	exit("Do not access this file directly.");

if (!class_exists('S2emailRepository')) {
	/**
	 * Repository class
	 */
	class S2emailRepository
	{
		public $wp_db;
		private $_s2email_pending_table;
		private $_s2email_template_table;
		private $_s2email_sent_table;
		public  $get_num_rows = 0;

		public function __construct()
		{
			global $wpdb;

			$this->wp_db = $wpdb;
			$this->_s2email_template_table 	= $wpdb->prefix .S2emailCore::EMAILS_TABLE;
			$this->_s2email_pending_table 	= $wpdb->prefix .S2emailCore::PENDING_EMAILS_TABLE;
			$this->_s2email_sent_table 		= $wpdb->prefix .S2emailCore::SENT_EMAILS_TABLE;
			$now 			  = new DateTime('now');
			$this->created_at = $now->format('Y-m-d H:i:s');
		}

		/**
		 * Insert single pending mail
		 */
		public function persistSinglePendingMail($id_user, $templateMailObj, $pending_type, $verify_ccaps = true)
		{
			if (empty($id_user) || !is_object($templateMailObj) || empty($pending_type)) {
				return false;
			}

			$user_eot = null;
			$subtract_eot = false;
			// we need user EOT in case of expiration only
			if (in_array($pending_type, array('before_exp','after_exp'))) {
				// get usermeta
				$user_eot = get_user_meta((int)$id_user, $this->wp_db->prefix.'s2member_auto_eot_time', true);

				if (!$user_eot) {
					return false;
				}

				if ($pending_type == 'before_exp') {
					$subtract_eot = true;
				}
			}

			if ($verify_ccaps) {
				// check user ccaps against template ccaps
				if (!S2emailCore::checkUserEligibleCcaps($templateMailObj, $id_user)) {
					return false;
				}
			}

			// calculate send_mail_at field based on user_eot
			$send_mail_at = $this->calculateSendingMail($templateMailObj->id, $user_eot, $subtract_eot, $id_user);

			// check duplicate emails
			if (!$send_mail_at 
			    || S2emailRepository::checkUserPendingMailByIdByType((int)$id_user, $pending_type, (int)$templateMailObj->id)) {
				return false;
			}

			$data = array(
				'id_user' 		=> (int)$id_user,
				'id_mail' 		=> (int)$templateMailObj->id, 
				'mail_type' 	=> $pending_type,
				'user_eot' 		=> $user_eot,
				'send_mail_at' 	=> $send_mail_at,
				'created_at' 	=> $this->created_at
			);

			$this->wp_db->insert( $this->_s2email_pending_table, 
				$data, array('%d', '%d', '%s', '%s', '%s', '%s') 
			);

			return $this->wp_db->insert_id;
		}

		/**
		 * Get template mail by schedule type
		 */
		public function getTemplateMailsByScheduleType($schedule_type, $status = 1, $active = 1)
		{
			return $this->getTemplateMails((int)$status, (int)$active, array(
			            ' AND `type_of_schedule` = '. (int)$schedule_type
			        ));
		}

		/**
		 * Get single template mail by id
		 */
		public function getTemplateMailById($id_tpl_mail, $status = 1, $active = 1)
		{
			$template_mail = $this->getTemplateMails((int)$status, (int)$active, array(
			            ' AND `id` = '. (int)$id_tpl_mail
			        ));

			return (count($template_mail) > 0 ? $template_mail[0] : false);
		}

		/**
		 * Select single template mail by id
		 */
		public static function getStaticTemplateMailById($id)
		{
			global $wpdb;

			return $wpdb->get_row("SELECT * FROM {$wpdb->prefix}". S2emailCore::EMAILS_TABLE .' WHERE `id` = '. (int)$id);
		}

		/**
		 * Get template mails
		 */
		public function getTemplateMails($status = 1, $active = 1, $conditions = array(), $output = OBJECT)
		{
			$sql = 'SELECT * FROM '. $this->_s2email_template_table . ' WHERE `active` = '. (int)$active .' AND `status` =  '. (int)$status . (count($conditions) > 0 ? implode(' ', $conditions) : '');

			$results = $this->wp_db->get_results( $sql, $output );
			$this->get_num_rows = $this->wp_db->num_rows;

			return $results; 
		}

		/**
		 * Get last template mail
		 */
		public static function getLastTemplateMail()
		{
			global $wpdb;
			$sql = 'SELECT * FROM '. $wpdb->prefix . S2emailCore::EMAILS_TABLE . ' 
				ORDER BY `id` DESC';

			return $wpdb->get_row( $sql );
		}

		/**
		 * Persist Pending mails
		 */
		public function persistPendingMail($user_id, $mail_type, $pending_type, $verify_ccaps = true)
		{
			$getMailTemplates = $this->getTemplateMailsByScheduleType((int)$mail_type);

			if ($num_rows = (int)$this->get_num_rows) {
				if ($num_rows > 1) {
					foreach ($getMailTemplates as $templateMailObj) {
						$this->persistSinglePendingMail((int)$user_id, $templateMailObj, $pending_type, $verify_ccaps);
					}
				} else {
					$templateMailObj = $getMailTemplates[0];
					$this->persistSinglePendingMail((int)$user_id, $templateMailObj, $pending_type, $verify_ccaps);
				}
			}
		}

		/**
		 * Select user's pending mails data
		 */
		public function getUserPendingMails($id_user, $status = null, $mail_type = null, $created_at = null, $created_at_format = '%Y-%m-%d', $id_mail = null)
		{
			$sql = "SELECT * FROM {$this->_s2email_pending_table} WHERE `id_user` = ". (int)$id_user . ($id_mail ? " AND `id_mail` = ". (int)$id_mail : '');
			if (null !== $status) {
				if (is_array($status)) {
					$sql .= " AND `status` IN (". implode(',', $status) .')';
				} else {
					$sql .= " AND `status` = ". (int)$status;
				}
			}
			$sql .= ($mail_type && count($mail_type) ? ' AND `mail_type` IN ('. implode(', ', $mail_type) .')' : '').
			($created_at ? " AND DATE_FORMAT(`created_at`,'{$created_at_format}') = '". esc_sql($created_at) ."'" : '');
			$sql .= ' AND `send_mail_at` >= "'. date('Y-m-d H:i:s') .'"';
			$results = $this->wp_db->get_results($sql);
			$this->get_num_rows = $this->wp_db->num_rows;

			return $results;
		}

		/**
		 * Select user's pending mail by mail_type
		 */
		public static function checkUserPendingMailByIdByType($id_user, $mail_type, $id_mail)
		{
			global $wpdb;

			return $wpdb->get_var("SELECT id FROM {$wpdb->prefix}". S2emailCore::PENDING_EMAILS_TABLE ." WHERE `id_user` = ". (int)$id_user .' AND `status` = 0 AND `mail_type` = "'. $mail_type .'" AND `id_mail` = '. (int)$id_mail
			);
		}

		/**
		 * Select pending mails data
		 */
		public static function getPendingMails($status = null, $start = null, $limit = null)
		{
			global $wpdb;

			$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}". S2emailCore::PENDING_EMAILS_TABLE . (($status || $status === 0) ? " WHERE `status` = ". (int)$status : ''). 
			       	' ORDER BY `send_mail_at` ASC'. 
			       	($limit ? ' LIMIT '. (int)$start .', '. (int)$limit : '')
			    );

			return array('obj' => $results, 'num_rows' => $wpdb->num_rows);
		}

		/**
		 * Select single pending mail by id
		 */
		public static function getPendingMailById($id)
		{
			global $wpdb;

			return $wpdb->get_row("SELECT * FROM {$wpdb->prefix}". S2emailCore::PENDING_EMAILS_TABLE .' WHERE `id` = '. (int)$id);
		}

		/**
		 * Update pending mails data by user
		 */
		public function updatePendingMailsStatusByUser($status, $id_user, $eot_update = false)
		{
			$sql = "UPDATE {$this->_s2email_pending_table}
				SET `status` = %d 
				WHERE `id_user` = %d AND `send_mail_at` >= %s";

			if ($eot_update) {
				$sql .= ' AND mail_type != "role_update"';
			}

			return $this->wp_db->query($this->wp_db->prepare($sql, $status, $id_user, date('Y-m-d H:i:s')
			));
		}

		/**
		 * Update pending mails data by id
		 */
		public function updatePendingMailsById($status, $id_user)
		{
			return $this->wp_db->query($this->wp_db->prepare("
					UPDATE {$this->_s2email_pending_table}
					SET `status` = %d WHERE `id_user` = %d AND `send_mail_at` >= %s
				", 
			    $status, $id_user, date('Y-m-d H:i:s')
			));
		}

		/**
		 * Update pending mails data
		 */
		public function updatePendingMailsByMailId($id_mail, $status)
		{
			$sql = "UPDATE {$this->_s2email_pending_table} SET `status` = %d 
					WHERE `send_mail_at` >= %s AND `status` <> 1";

			if (is_array($id_mail)) {
				$sql .= ' AND `id_mail` IN ('. implode(',', $id_mail) . ')';
			} else {
				$sql .= ' AND `id_mail` = '. (int)$id_mail;
			}

			return $this->wp_db->query(
			    $this->wp_db->prepare($sql, $status, date('Y-m-d H:i:s'))
			);
		}

		/**
		 * Update pending mails data by id
		 */
		public function updatePendingMailToSentById($id)
		{
			return $this->wp_db->query($this->wp_db->prepare("
				UPDATE {$this->_s2email_pending_table} SET `status` = 1 WHERE `id` = %d
				", $id
			));
		}

		/**
		 * Update pending mails data
		 */
		public function updatePendingMailsByData($id_mail, $id_user, $status, $user_eot, $send_mail_at)
		{
			$preparedQry = $this->wp_db->prepare("
					UPDATE {$this->_s2email_pending_table}
					SET `status` = %d, `user_eot` = %s, `send_mail_at` = %s 
					WHERE `id_mail` = %d AND `id_user` = %d AND `send_mail_at` >= %s
				", $status, $user_eot, $send_mail_at, $id_mail, $id_user, date('Y-m-d H:i:s')
			);
			return $this->wp_db->query($preparedQry);
		}

		/**
		 * Update pending mails data
		 */
		public static function updateStaticPendingMails($data, $where)
		{
			global $wpdb;

			return $wpdb->update($wpdb->prefix.S2emailCore::PENDING_EMAILS_TABLE, $data, $where);
		}

		/**
		 * Delete user's pending mails
		 */
		public function deleteUserPendingMails($id_user, $conditions = array())
		{
			$_conditions = array( 'id_user' => (int)$id_user );

			if (is_array($conditions) && count($conditions)) {
				$_conditions = array_merge($_conditions, $conditions);
			}

			return $this->wp_db->delete( $this->_s2email_pending_table, $_conditions );
		}

		/**
		 * Delete all pending mails related to a template
		 */
		public function deletePendingMailsByTemplateId($id_mail)
		{
			return $this->wp_db->delete($this->_s2email_pending_table, array('id_mail' => (int)$id_mail ), array( '%d' ) );
		}

		/**
		 * Update template table counter
		 */
		public static function updateStaticTemplateMailCounter($id)
		{
			global $wpdb;

			return $wpdb->query("UPDATE {$wpdb->prefix}". S2emailCore::EMAILS_TABLE .' 
			    SET counter = counter+1 WHERE `id` = '. (int)$id);
		}

		/**
		 * Persist data into sent mails
		 */
		public static function persistSentMail($id_mail, $id_user, $title, $subject, $user_email, $email_body, $expire_time = null)
		{
			global $wpdb;

			$wpdb->insert($wpdb->prefix . S2emailCore::SENT_EMAILS_TABLE, 
				array(
					'mail_id' 		=> $id_mail, 
					'expire_time' 	=> $expire_time,
					'user_id' 		=> $id_user,
					'sent_time' 	=> date('Y-m-d H:i:s'),
					'title' 		=> $title,
					'subject' 		=> $subject,
					'email' 		=> $user_email,
					'content' 		=> $email_body
				) 
			);
		}

		/**
		 * Calculate send email time
		 */
		public function calculateSendingMail($id_tpl_mail, $user_eot = null, $subtract_eot = false, $user_id = false)
		{
			$getTemplateMail = $this->getTemplateMailById($id_tpl_mail);
			
			if (!$getTemplateMail) {
				return false;
			}

			if (get_option('s2emails_timezone')) {
				$timeToSend = new DateTime("now", new DateTimeZone(get_option('s2emails_timezone')));
			} else {
				$timeToSend = new DateTime("now");
			}

			if (null !== $user_eot) {
				$timeToSend->setTimestamp(trim($user_eot));
			}

			$addsub = (false !== $subtract_eot ? '-' : '+');
			$send_mail_at = '';
			$format = 'Y-m-d H:i:s';

			if ($getTemplateMail->time_type == 1) {
				$days  = (int)$getTemplateMail->days;
				$hours = (int)$getTemplateMail->hours;
				$timeToSend->modify("{$addsub}{$days} days");
				$timeToSend->modify("{$addsub}{$hours} hours");
				$send_mail_at = $timeToSend->format($format);
			} else {
				$hours_2 = $getTemplateMail->hours_2;
				$days_2  = (int)$getTemplateMail->days_2;
				$timeToSend->modify("{$addsub}{$days_2} days");
				$send_mail_at = $timeToSend->format('Y-m-d') .' '. $hours_2;
			}

			$createNewDate	= new DateTime("now");
			$trickyDate 	= new DateTime($send_mail_at);
			// here we add one more day 
			if ($createNewDate->getTimeStamp() > $trickyDate->getTimeStamp()) {
				$trickyDate->modify('+1 day');
				$send_mail_at = $trickyDate->format($format);
			}

			// we check if the user id is existing with after reg type, then we calculate if the user is registered in the past to adapt pending mail sending date
			if (false !== $user_id
			    && $getTemplateMail->type_of_schedule == 1) {
				$user_registered = new DateTime((new WP_User($user_id))->data->user_registered);
				// we check if the user registration date is in the past
				if ($user_registered->getTimeStamp() < $createNewDate->getTimeStamp()) {
					$date_diff = $user_registered->diff($createNewDate);
					// recalculate sending time
					if ($getTemplateMail->time_type == 1) {
						$days  = (int)$date_diff->days;
						$hours = (int)$getTemplateMail->hours;
						$timeToSend->modify("-{$days} days");
						$timeToSend->modify("-{$hours} hours");
						$changed_time = $timeToSend->format($format);
					} else {
						$hours_2 = $getTemplateMail->hours_2;
						$days_2  = (int)$date_diff->days;
						$timeToSend->modify("-{$days_2} days");
						$changed_time = $timeToSend->format('Y-m-d') .' '. $hours_2;
					}
					// if the sending time is not in the past
					if ($timeToSend->getTimeStamp() >= $createNewDate->getTimeStamp()) {
						$send_mail_at = $changed_time;
					}
				}
			}

			return $send_mail_at;
		}

		/**
		 * Fetch users 
		 */
		public function getUsers()
		{
			$users = $this->wp_db->get_results('SELECT `ID`, `user_registered` FROM `'. $this->wp_db->prefix .'users`');
			$this->get_num_rows = $this->wp_db->num_rows;

			return $users;
		}

		public function getBeAfExpPendingMails($status = 0, $mail_types = null)
		{
			$sql = 'SELECT * FROM '. $this->_s2email_pending_table . ' WHERE `status` = '. (int)$status
			.(null !== $mail_types ? ' AND `mail_type` IN ('. $mail_types .')' : '');
			$results = $this->wp_db->get_results( $sql );

			return $results;
		}

		/**
		 * Create custom sql query
		 */
		public function createSqlQuery($sql, $id_user, $status = null)
		{
			if (null !== $status) {
				$prepare = $this->wp_db->prepare($sql, $id_user, $status);
			} else {
				$prepare = $this->wp_db->prepare($sql, $id_user);
			}

			return $this->wp_db->query( $prepare ); 
		}

		/**
		 * get int value from string value for schedule types
		 */
		public static function getScheduleTypeFromStrToInt($type)
		{
			$int_type = '';

			switch ($type) {
				case 'after_reg':
					$int_type = 1;
					break;
				case 'before_exp':
					$int_type = 2;
					break;
				case 'after_exp':
					$int_type = 3;
					break;
				case 'shortcode':
					$int_type = 4;
					break;
				case 'role_update':
					$int_type = 5;
					break;
				case 'after_cancel':
					$int_type = 6;
					break;
			}

			return $int_type;
		}

		/**
		 * get str value from int value for schedule types
		 */
		public static function getScheduleTypeFromIntToStr($type)
		{
			$str_type = '';

			switch ($type) {
				case 1:
					$str_type = 'after_reg';
					break;
				case 2:
					$str_type = 'before_exp';
					break;
				case 3:
					$str_type = 'after_exp';
					break;
				case 4:
					$str_type = 'shortcode';
					break;
				case 5:
					$str_type = 'role_update';
					break;
				case 6:
					$str_type = 'after_cancel';
					break;
			}

			return $str_type;
		}
	}
}