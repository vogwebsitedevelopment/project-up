<?php
if(!defined('WPINC')) // MUST have WordPress.
    exit("Do not access this file directly.");

if ( !class_exists('S2pluginChecker') ) {
    /**
    * S2plugin checker class
    */
    class S2pluginChecker
    {
        private $_new_version;
        private $_old_version;
        private $_plugin_name;
        private $_plugin_version_name;
        private $_upgrade_path;

        public function __construct($new_version, $upgrade_path, $plugin_name)
        {
            $this->_new_version  = $new_version;
            $this->_upgrade_path = rtrim($upgrade_path, '/');
            $this->_plugin_name = $plugin_name;
            $this->_plugin_version_name = $plugin_name .'_version';
        }

        /**
         * Check if the plugin's version exists in db options table 
         * or not. If it doesn't exist, persist it.
         */
        public function isDbOldVersion()
        {
            if ( !get_option( $this->_plugin_version_name ) ) {
                $this->addDbVersion();

            	return true;
            }

        	$old_version = get_option($this->_plugin_version_name);
            if ( version_compare( $this->_new_version, $old_version, '>' ) ) {
                $this->_old_version = $old_version;
            	return true;
            }

            return false;
        }

        /**
         * Update plugin version in the options table
         */
        public function updateDbVersion()
        {
        	update_option( $this->_plugin_version_name, $this->_new_version );
        }

        /**
         * Update plugin version in the options table
         */
        public function addDbVersion()
        {
        	add_option( $this->_plugin_version_name, $this->_new_version );
        }

        /**
         * 
         */
        public function runUpdater()
        {
        	// check if the plugin has already this plugin version of not
        	if ( !$this->isDbOldVersion() || !current_user_can('update_plugins') ) {
        		return false;
        	}

        	if ( !file_exists($this->_upgrade_path) || !is_dir($this->_upgrade_path) ) {
        		throw new Exception(sprintf('Directory %s does not exist or is not a directory', $this->_upgrade_path), 1);
        		
        	}

            $_old_ver = (str_replace('.', '', $this->_old_version) + 1);
            $_new_ver = str_replace('.', '', $this->_new_version);
            $_range = range($_old_ver, $_new_ver);

            foreach ($_range as $_v) {
            	if ( !$this->runUpgrader($_v) ) {
                    continue;
                }
            }

            // update Db version
            $this->updateDbVersion();

            return true;
        }

        public function runUpgrader($version)
        {
            $upgrade_function = 'upgrade_'. str_replace('.', '_', $this->addVersionDots($version));
            $file_path = $this->_upgrade_path .'/'. $upgrade_function .'.php';

            if ( !file_exists($file_path) ) {
                return false;
            }

            // include the function file
            require_once $file_path;

            // if function exists execute it
            if (function_exists($upgrade_function)) {
                $upgrade_function();
            }

            return true;
        }

        private function addVersionDots($version)
        {
            $splitWord = str_split($version);
            $_v = '';
          
            foreach ($splitWord as $k => $wrd) {
                $_v .= ($k > 0 ? '.' : '') . $wrd;
            }
          
            return $_v;
        }
    }
}