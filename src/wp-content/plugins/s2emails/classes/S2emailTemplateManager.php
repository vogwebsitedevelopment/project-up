<?php
if(!defined('WPINC')) // MUST have WordPress.
	exit("Do not access this file directly.");

if (!class_exists('S2emailTemplateManager')) {
	/**
	* Manage Template files
	*/
	class S2emailTemplateManager
	{
		/**
		 * List of automated emails
		 */
		public static function s2emailsListOfAutomatedEmails(array $options)
		{
			extract($options);
			include(S2EMAILS_TPL_DIR .'_partials/automated_list.php');
		}

		/**
		 * List of automated emails
		 */
		public static function s2emailsManipulateListOfAutomatedEmails()
		{
			include(S2EMAILS_TPL_DIR .'emails_list.php');
		}

		/**
		 * Getting started section
		 */
		public static function s2emailsGettingStarted()
		{
			include(S2EMAILS_TPL_DIR .'getting_started.php');
		}

		/**
		 * Add/Edit automated emails
		 */
		public static function addEditAutomatedEmail()
		{
			include(S2EMAILS_TPL_DIR .'add_edit_automated_email.php');
		}

		/**
		 * Sent email list
		 */
		public static function s2emailSentList()
		{
			include(S2EMAILS_TPL_DIR .'sent_emails_list.php');
		}

		/**
		 * General options
		 */
		public static function s2emailsGeneralOtptions()
		{
			S2emailCore::updpateAutoEmailsOpts();
			S2emailCore::updpateExtraTimeEotOpt();
			include(S2EMAILS_TPL_DIR .'general_options.php');
		}

		/**
		 * Get Pending mails list
		 */
		public static function s2emailPeningList()
		{
			include(S2EMAILS_TPL_DIR .'pending_emails_list.php');
		}
	}
}