<?php
if(!defined('WPINC')) // MUST have WordPress.
	exit("Do not access this file directly.");

if (!class_exists('S2emailCore')) {
	/**
	* 
	*/
	class S2emailCore
	{
		const EMAILS_TABLE 			= 's2emails';
		const PENDING_EMAILS_TABLE 	= 's2emails_pending';
		const SENT_EMAILS_TABLE 	= 's2emails_sent';
		const S2EMAILS_PLG_NAME		= 's2emails';
		const S2EMAILS_PLG_SLUG		= 'wp-plugin-s2emails';
		const S2EMAILS_MENU_NAME	= 's2Emails (Pro)';
		const S2EMAILS_VERSION 		= '2.1.4';
		const S2EMAILS_CAP 			= 'manage_options';
		public $initializer;
		public $s2emails_repo;

		public function __construct(
			S2emailInitializer $initializer,
		 	S2emailRepository $s2emails_repo
		)
		{
			$this->initializer  = $initializer;
			$this->s2emails_repo = $s2emails_repo;
			$this->initializer->setS2emailsRepo($s2emails_repo);

			$mofile_name = PLUGIN_PATH .'languages/'. self::S2EMAILS_PLG_NAME .'-'. get_locale() .'.mo';
			if (file_exists($mofile_name)) {
				load_textdomain( self::S2EMAILS_PLG_NAME, $mofile_name );
				load_plugin_textdomain(self::S2EMAILS_PLG_NAME, false, PLUGIN_PATH . 'languages/');
			}
		}

		public function run()
		{
			// initializer starts
			$this->initializer->init();
			// set local time based on Wordpress Timezone
			date_default_timezone_set(
			    (get_option('s2emails_timezone') ?: date_default_timezone_get()));

			// crons
			add_filter( 'cron_schedules', array( $this, 's2emails_schedule' ));
			add_action( 'call_for_mail', array($this, 'cron_job'));

			if( !wp_next_scheduled( 'call_for_mail' ) ) {
				$s2emails_cron_job_duration = (get_option('s2emails_cron_job_duration') ?: 10);
				wp_schedule_event( time(), 's2email_every_'. $s2emails_cron_job_duration .'_minutes', 'call_for_mail' ); 	 
			}

			// listen to role changes
			add_action('set_user_role', array($this, 'getUserChangedRole'), 20, 4);
			// listen to new registration, I put 20 as low priority to give time for s2member to insert the eot for the current user
			add_action('user_register', array($this, 'checkNewUserRegistration'), 20, 1 );
			// listen to profile update
			add_action('profile_update', array($this, 'getUpdatedUserProfile'), 25, 3);
			// listen to deleted users delete user hook
			add_action('delete_user', array($this, 'getDeletedUser'), 10, 1);
			// listen to new persisted email
			// add_action('s2emails_admin_inserted_auto_mail_before', array($this, 'checkNewPersistedMail'), 1, 2);
			add_action('s2emails_admin_inserted_auto_mail', array($this, 'checkNewPersistedMail'), 1, 2);
			// listen to an updated auto mail
			// add_action('s2emails_admin_updated_auto_mail', array($this, 'checkNewPersistedMail'), 1, 2);
			/*add_filter( 'pre_user_login', function( $user ) {
			    self::dump([ current_filter()." works fine", $user], true );
			} );*/

			// check for unsubscribing users from getting automated emails
			$this->_unsubscribe_user();

			// auto emails options hook
			add_action('s2emails_admin_auto_mail_opts', array($this, 'checkAutoEmailsOptions'), 1, 1);
			// hook extra time for users' EOT option
			add_action('s2emails_extra_time_eot_opt', array($this, 'checkExtraTimeEotOption'), 1, 1);
		}

		/**
		 * Get new user registration to set up their pending mails
		 */
		public function checkNewUserRegistration($user_id)
		{
			// create pending mail to send before/after expiration if EOT is set
			$user_eot = get_user_meta((int)$user_id, $this->s2emails_repo->wp_db->prefix.'s2member_auto_eot_time', true);
			// get user capabilities
			// $user_cap = get_user_meta((int)$user_id, $this->s2emails_repo->wp_db->prefix.'capabilities', true);
			$current_user = get_user_by('ID', $user_id);
			sort($current_user->roles);
			$_usr_role 	  = $current_user->roles[0];

			if ( $_usr_role != 'subscriber' ) {
				if (!$user_eot || !preg_match('/^s2member_level\d+$/', $_usr_role)) {
					return;
				}
			}

			// create pending mail to send after registration
			$this->s2emails_repo->persistPendingMail($user_id, 1, 'after_reg');

			// before expiration
			$this->s2emails_repo->persistPendingMail($user_id, 2, 'before_exp');

			// after expiration
			$this->s2emails_repo->persistPendingMail($user_id, 3, 'after_exp');
		}

		/**
		 * Get user when profile is updated
		 */
		public function getUpdatedUserProfile($user_id, $old_user_data)
		{
			global $wpdb;

			$isRemainderEnabled = get_user_meta((int)$user_id, $wpdb->prefix.'s2member_reminders_enable', true);

			if ($isRemainderEnabled == '0') {
				$this->s2emails_repo->updatePendingMailsStatusByUser(2, (int)$user_id);
				return false;
			} else {
				$this->s2emails_repo->updatePendingMailsStatusByUser(0, (int)$user_id);
			}

			// get EOT of this user, if he hasn't one, return false
			$getUserEot = get_user_meta((int)$user_id, $wpdb->prefix.'s2member_auto_eot_time', true);
			// update pending mails to deactivated them for the current user
			if (!$getUserEot) {
				$this->s2emails_repo->updatePendingMailsStatusByUser(2, (int)$user_id, true);
				return false;
			}

			$getUserPendingMailData = $this->s2emails_repo->getUserPendingMails($user_id, null, array('"after_exp"', '"before_exp"', '"shortcode"'));

			if (!$getUserPendingMailData)  {
				// get All templates
				$getTemplates = $this->s2emails_repo->getTemplateMails(1, 1, array(' AND `type_of_schedule` NOT IN (4, 1, 5, 6)'));
				$current_user = get_user_by('ID', $user_id);
				sort($current_user->roles);
				$user_role = $current_user->roles[0];

				foreach ($getTemplates as $getTemplate) {
					$pending_mail = S2emailRepository::getScheduleTypeFromIntToStr($getTemplate->type_of_schedule);
					$exp_roles = explode('|', $getTemplate->roles);
					$persist = false;

					foreach ($exp_roles as $_r) {
						if ($user_role == $_r) {
							$persist = true;
							break;
						}
					}

					if (false === $persist) {
						continue;
					}

					$this->s2emails_repo->persistSinglePendingMail($user_id, $getTemplate, $pending_mail);
				}

				return false;
			}

			$getUserPendingMailEot = $getUserPendingMailData[0]->user_eot;
			if ($getUserPendingMailEot === $getUserEot) {
				return false;
			}

			foreach ($getUserPendingMailData as $pendingMail) {
				$subtract_eot = false;

				// check for user CCAPs each time profile is updated
				$getTemplate  = S2emailRepository::getStaticTemplateMailById($pendingMail->id_mail);

				if (!self::checkUserEligibleCcaps($getTemplate, $user_id)) {
					return false;
				}

				if ($pendingMail->mail_type == 'before_exp') {
					$subtract_eot = true;
				}

				// calculate send_mail_at field based on user_eot
				$send_mail_at = $this->s2emails_repo->calculateSendingMail($pendingMail->id_mail, $getUserEot, $subtract_eot);

				$this->s2emails_repo->updatePendingMailsByData($pendingMail->id_mail, $user_id, 0, $getUserEot, $send_mail_at);
			}
		}

		/**
		 * Get use role changement
		 */
		public function getUserChangedRole($user_id, $role = null, $old_roles = null)
		{
			global $wpdb;

			// getUserChangedRole() function is called when a new user is manually created. This condition prevents creating pending "changeRole" emails for new users 
			if (empty($old_roles)) {
				return false;
			}

			$current_user = get_user_by('ID', $user_id);
			sort($current_user->roles);
			$get_current_user_new_role = $role;

			// check real roles change
			if (($get_current_user_new_role
				&& $old_roles[0] == $get_current_user_new_role)
				|| !$get_current_user_new_role || !$old_roles[0]) {
				return false;
			}

			// regenerate all pending emails for users' roles
			if ($get_current_user_new_role != 'subscriber') {
				// Remove all current active pending emails
				$this->s2emails_repo->createSqlQuery("DELETE FROM {$wpdb->prefix}". S2emailCore::PENDING_EMAILS_TABLE .' WHERE id_user = %d AND status = 0 AND mail_type NOT IN ("role_update", "shortcode", "after_cancel")', $user_id);

				// create pending mail to send after registration
				$this->s2emails_repo->persistPendingMail($user_id, 1, 'after_reg');
				// before expiration
				$this->s2emails_repo->persistPendingMail($user_id, 2, 'before_exp');
				// after expiration
				$this->s2emails_repo->persistPendingMail($user_id, 3, 'after_exp');

			} else {
				// Remove all current active welcome/before expiration pending emails
				$this->s2emails_repo->createSqlQuery("DELETE FROM {$wpdb->prefix}". S2emailCore::PENDING_EMAILS_TABLE .' WHERE id_user = %d AND status = 0 AND mail_type NOT IN ("after_exp", "shortcode", "after_cancel")', $user_id);
			}

			$conditions = array();
			$conditions[] = ' AND `type_of_schedule` = 5';
			$conditions[] = ' AND roles LIKE "%'. $old_roles[0] .'%" AND (new_roles LIKE "%'. $get_current_user_new_role .'%" AND new_roles IS NOT NULL)';
			$getUserRoleChangeTpls = $this->s2emails_repo->getTemplateMails(1, 1, $conditions);

			// persist update templates
			if ( count($getUserRoleChangeTpls) && $old_roles[0] ) {
				foreach ($getUserRoleChangeTpls as $getTpl) {
					$this->s2emails_repo->persistSinglePendingMail($user_id, $getTpl, 'role_update', false);
				}
			}

			return false;
		}

		/**
		 * Trim ccaps
		 */
		public static function escapeCcapsSpaces($ccap)
		{
			return trim($ccap);
		}

		/**
		 * Catch the deletion if a user
		 */
		public function getDeletedUser($user_id)
		{
			$this->s2emails_repo->deleteUserPendingMails($user_id);
		}

		/**
		 * Schedule job crons
		 */
		public function s2emails_schedule($schedules)
		{
			$s2emails_cron_job_duration = get_option('s2emails_cron_job_duration');
			
			if ($s2emails_cron_job_duration == 30) {
				$schedules['s2email_every_30_minutes'] = array(
					'interval' => 1800,
					'display' => 'Every 30 Minutes'
				);
			} elseif ($s2emails_cron_job_duration == 20) {
				$schedules['s2email_every_20_minutes'] = array(
					'interval' => 1200,
					'display' => 'Every 20 Minutes'
				);
			} else {
				$schedules['s2email_every_10_minutes'] = array(
					'interval' => 600,
					'display' => 'Every 10 Minutes'
				);
			}

		 	return $schedules;
		}

		/**
		 * Launch the cron job
		 */
		public function cron_job()
		{
			global $wpdb;

			$from_email = get_option('s2emails_from_email');
			$from_title =  get_option('s2emails_from_title');
			$copy_email = get_option('s2emails_copy_email');
			$currentTime = date('Y-m-d h:iA');
			$currentTimeStr = strtotime($currentTime);

			/* if the FROM email is not set, nothing is sent */
			if($from_email == false or $from_email == '')
				exit;

			$getPendingMails = S2emailRepository::getPendingMails(0);

			if ($getPendingMails['num_rows'])
			foreach($getPendingMails['obj'] as $mail) {
				$id_user  = $mail->id_user;
				$id_mail  = $mail->id_mail;
				$send_mail_at  = strtotime($mail->send_mail_at);
				$userData = get_user_by('ID', $id_user);
				$userMeta = get_user_meta($id_user);
				sort($userData->roles);
				$user_role  = $userData->roles[0];
				$first_name = $userMeta['first_name'][0];
				$last_name 	= $userMeta['last_name'][0];
				$username 	= $userMeta['nickname'][0];
				$display_name = $userData->data->display_name;

				// prepare headers
				$headers = array();
				/* all emails can be sent also to a BCC, if it's set. (for testing purposes of the website admin) */
				if ($copy_email != false and $copy_email != '') {
					$headers[] = 'Bcc: '.$copy_email.'';
				}

				$headers[] = 'From: '.$from_title.' <'.$from_email.'>';
				$headers[] = 'Content-Type: text/html; charset=UTF-8';

				/* EOT can be also empty if the user is subscribed. In that case we simple cannot send any expiratoin emails */
				$user_eot = get_user_meta($id_user, $wpdb->prefix .'s2member_auto_eot_time', true);
				/* registration date */
				$user_tor = $userData->data->user_registered;
				$date_of_expiration = '';
				
				if (!empty($user_eot)) {
					$date_of_expiration = date('Y-m-d h:iA', $user_eot);
				}

				$date_of_registration = date('Y-m-d h:iA', strtotime($user_tor));
				$isRemainderEnabled = get_user_meta($id_user, $wpdb->prefix.'s2member_reminders_enable', true);
				
				/* send emails only if "reminders" are enabled. This value is added by s2member. If it's off, s2emails and also s2member will not send any emails to the user */
				if($isRemainderEnabled == '0')
					continue;

				$getTemplate = S2emailRepository::getStaticTemplateMailById($id_mail);
				$to 		 = $userData->data->user_email;
				$subject 	 = $getTemplate->subject;
				$title 		 = $getTemplate->title;

				// check which emails should be sent
				if ($currentTimeStr >= $send_mail_at) {
					// check if template has cc emails add them
					if ( $getTemplate->cc_emails
					    && !empty($getTemplate->cc_emails) ) {
						$headers[] = 'Cc: '. $getTemplate->cc_emails;
					}

					// set message body
					if ($getTemplate->content_type == 1) {
						$message_body = html_entity_decode(nl2br($getTemplate->content));
					} else {
						$message_body = html_entity_decode($getTemplate->content);
					}

					// retrieve reset password link
					$resetPasswordUrl = $this->_s2emails_reset_password_url($userData->data->user_email);

					/* create the body of the automated email to the user */
					$mail_body = str_replace(
						array(
						    '{first_name}',
							'{last_name}',
							'{username}',
							'{display_name}',
							'{date_of_registration}',
							'{date_of_expiration}',
							'{user_role}',
							'{new_password}'
						),
						array(
						    $first_name, 
							$last_name, 
							$username, 
							$display_name, 
							$date_of_registration, 
							$date_of_expiration, 
							$user_role,
							$resetPasswordUrl
						),
						$message_body);

					if (in_array($getTemplate->type_of_schedule, array(2,3))) {
						S2emailRepository::persistSentMail($id_mail, $id_user, $title, $subject, $to, $message_body, $date_of_expiration);
					} else {
						S2emailRepository::persistSentMail($id_mail, $id_user, $title, $subject, $to, $message_body);
					}

					// set template counter
					S2emailRepository::updateStaticTemplateMailCounter($id_mail);
					// update pending mail to sent mail
					$this->s2emails_repo->updatePendingMailToSentById($mail->id);

					// check if unsubscribe placeholder is set
					if (false !== strpos($mail_body, '{unsubscribe}')
					    && false !== strpos($mail_body, '{/unsubscribe}')) {
					  	$unsubscribe_start = (strpos($mail_body, '{unsubscribe}') + strlen('{unsubscribe}'));
					  	$unsubscribe_end = strpos($mail_body, '{/');
					  	$getUnsubscribeText = substr($mail_body, $unsubscribe_start, ($unsubscribe_end - $unsubscribe_start));

					  	// remove unsubscribe text as we no longer need it
					  	$mail_body = preg_replace('#\{unsubscribe\}(.*?)\{/unsubscribe\}#i', '', $mail_body);

					  	// generate unsubscribe token
						$token = wp_generate_password( 16, false );

						// update user unsubscribe token
						update_user_meta( $id_user, $wpdb->prefix.'s2emails_unsubscribe_token', $token );

						// prepare unsubscribe link
					  	$mail_body .= '<a href="'. network_site_url("?u=$id_user&token=$token") .'">'. $getUnsubscribeText .'</a>';
					}

					wp_mail( $to, $subject, $mail_body, $headers );
				}
			}
		}

		/**
		 * Check user eligible against template ccaps 
		 */
		public static function checkUserEligibleCcaps($templateOjb, $user_id)
		{
			global $wpdb; 
			$userCcaps 	  = get_user_field('s2member_access_ccaps', $user_id);
			$rolesExplode = explode('|', $templateOjb->roles);
			$validated 	  = false;
			$current_user = get_user_by('ID', $user_id);;
			sort($current_user->roles);
			$_usr_role 	  = $current_user->roles[0];

			if (!count($rolesExplode)) {
				return true;
			}

			foreach ($rolesExplode as $exRole) {
				if (false === strpos($exRole, ':')) {
					// check if user role is not in
					if ($_usr_role != $exRole) {
						continue;
					}
					$validated = true;
					break;
				}
				list($_role, $_ccaps) = explode(':', $exRole);

				// check if user role is not in
				if ($_usr_role != $_role) {
					continue;
				}

				// template has no ccaps
				if (!count($_ccaps)) {
					$validated = true;
					break;
				}

				// check if user has no ccaps
				if (!count($userCcaps)) {
					break;
				}

				$ccaps = explode(',', $_ccaps);
				$intersectedCcaps = array_intersect(array_map(array('S2emailCore', 'escapeCcapsSpaces'), $ccaps), $userCcaps);

				if (count($intersectedCcaps) > 0) {
					$validated = true;
					break;
				}
			}

			return $validated;
		}

		/**
		 * Set timezones
		 */
		public static function s2emals_timezone($selected_timezone = null)
		{
			static $mo_loaded = false;

			$continents = array( 'Africa', 'America', 'Antarctica', 'Arctic', 'Asia', 'Atlantic', 'Australia', 'Europe', 'Indian', 'Pacific');

			// Load translations for continents and cities
			if ( !$mo_loaded ) {
				$locale = get_locale();
				$mofile = WP_LANG_DIR . '/continents-cities-' . $locale . '.mo';
				load_textdomain( 'continents-cities', $mofile );
				$mo_loaded = true;
			}
			
			$getTimeZoneOptions = array();
			foreach (timezone_identifiers_list() as $timezone) {
				$selected = ($timezone == $selected_timezone ? 'selected="selected" ' : '');
				
				$getTimeZoneOptions[] = '<option ' . $selected . 'value="' . esc_attr( $timezone ) . '">' . esc_html( $timezone ) . "</option>";

			}

			return implode("\n", $getTimeZoneOptions);
		}

		/**
		 * Check new persisted mail
		 */
		public function checkNewPersistedMail($id_mail, $data, $is_iterated = false, $send_mail_mode = null)
		{
			$type_of_schedule = (int)$data['type_of_schedule'];

			if (false === $is_iterated) {
				$send_mail_mode = (int)get_option( 's2emails_send_auto_email_to' );

				// if not set or mode set to only new members
				if ((!$send_mail_mode || $send_mail_mode == 1) 
				    // we can't prepare emails for shorcode mails, nor for user role / cancelation
				    || in_array($type_of_schedule, array(4,5,6))) {
					return;
				}
			} else {
				// we can't prepare emails for shorcode mails, nor for user role / cancelation
				if (in_array($type_of_schedule, array(4,5,6))) {
					return;
				}
			}

			$scheculeTypeAsString = $this->s2emails_repo->getScheduleTypeFromIntToStr($type_of_schedule);
			$eot_meta_key = $this->s2emails_repo->wp_db->prefix . "s2member_auto_eot_time";
			
			// get list of existing users
			$users = $this->s2emails_repo->getUsers();

			if (!$this->s2emails_repo->get_num_rows) {
				return;
			}

			if ($s2emails_timezone = get_option('s2emails_timezone')) {
				$today = new DateTime("now", new DateTimeZone($s2emails_timezone));
			} else {
				$today = new DateTime("now");
			}

			$email_date_obj = self::getBackDateFromTemplateData($data, false, $today);
			$get_template_obj = $this->s2emails_repo->getTemplateMailById($id_mail);

			// iterate through existing users to create them pending mails
			foreach ($users as $user) {
				// check user's registration time / user's registration time
				if ($type_of_schedule == 1 && (!$email_date_obj || $user->user_registered < $email_date_obj)) {
					continue;
				}

				// create pending mail to send before/after expiration if EOT is set
				$user_eot = get_user_meta((int)$user->ID, $eot_meta_key, true);
				// get user capabilities
				$user_cap = get_user_meta((int)$user->ID, $this->s2emails_repo->wp_db->prefix.'capabilities', true);
				$is_role_found = '';
				$is_ccap_found = false;

				if ($type_of_schedule != 1) {
					if (!$user_eot) {
						continue;
					}
				}

				// check roles first
				if (!isset($data['roles']) || !count($data['roles'])) {
					continue;
				}

				foreach ($data['roles'] as $role) {
					if (array_key_exists($role, $user_cap)) {
						$is_role_found = $role;
      					unset($user_cap[$role]);
						break;
					}
				}

				// if role not found, skip
				if (empty($is_role_found)) {
					continue;
				}

				$template_ccaps = $data[$is_role_found.'_ccaps'];

				if (!count($user_cap) 
				    && (isset($template_ccaps) && !empty($template_ccaps))) {
					continue;
				}

				if (!isset($template_ccaps) 
				    || !array_key_exists($template_ccaps, $data)) {	
					$is_ccap_found = true;
				} elseif (isset($template_ccaps)) {
					// check ccaps
					if (false !== strpos($template_ccaps, ',')) {
					    $mail_ccaps = explode(',', $template_ccaps);
					} else {
					    $mail_ccaps = array($template_ccaps);
					}

				    foreach ($mail_ccaps as $ccap) {
				    	$ccap = strtolower($ccap);
					    if (isset($user_cap['access_s2member_ccap_'.$ccap]) && $user_cap['access_s2member_ccap_'.$ccap] == 1) {
					          $is_ccap_found = true;
							break;
					    }
				    }
				}

				if (!$is_ccap_found) {
					continue;
				}

				// before expiration
				if ($type_of_schedule == 2) {
					if ($_s2emails_timezone = get_option('s2emails_timezone')) {
						$ueot = new DateTime("now", new DateTimeZone($_s2emails_timezone));
						$exp_time = new DateTime("now", new DateTimeZone($_s2emails_timezone));
					} else {
						$ueot = new DateTime("now");
						$exp_time = new DateTime("now");
					}

					$ueot->setTimestamp(trim($user_eot));

					if ((int)$data['time_type'] == 1) {
						$days  = (int)$data['days'];
						$hours = (int)$data['hours'];
						$exp_time->modify("+{$days} days");
						$exp_time->modify("+{$hours} hours");
					} else {
						$hours_2 = $data['hours_2'];
						$days_2  = (int)$data['days_2'];
						$exp_time->modify("+{$days_2} days");
						$exp_time = new DateTime($exp_time->format('Y-m-d') .' '. $hours_2);
					}
					
					if ($exp_time->getTimeStamp() > $ueot->getTimeStamp()) {
						continue;
					}
				}

				// persist new pending mail
				$this->s2emails_repo->persistSinglePendingMail($user->ID, $get_template_obj, $scheculeTypeAsString, false);
			}

			// update pending mail	
			if ((int)$id_mail) {
				$status = 2;
				if ((int)$data['active'] == 1) {
					$status = 0;
				}
			    $this->s2emails_repo->updatePendingMailsByMailId($id_mail, $status);
			}
		}

		/**
		 * For debugging purposes
		 */
		public static function dump($data, $die = false)
		{
			echo "<pre>";
			print_r($data);
			echo "</pre>";
			if ($die) die();
		}

		public static function getBackDateFromTemplateData($template_data, $return_as_obj = false, $date = null)
		{
			if (null === $date || !($date instanceof DateTime)) {
				if ($s2emails_timezone = get_option('s2emails_timezone')) {
					$date = new DateTime("now", new DateTimeZone($s2emails_timezone));
				} else {
					$date = new DateTime("now");
				}
			}

			if ((int)$template_data['time_type'] == 1) {
				$days 	  = (int)$template_data['days'];
				$days_str = (($days > 1) ? ' days' : ' day');
				$hours 	  = '';

				if ($_hours = (int)$template_data['hours']) {
					$hours = ' '. $_hours . (($_hours > 1) ? ' hours' : ' hour');
				}

				$date->setTimestamp(strtotime('-'. $days . $days_str . $hours));
			} else {
				$days 	  = (int)$template_data['days_2'];
				$days_str = (($days > 1) ? ' days' : ' day');
    			$split_hours = explode(':', $template_data['hours_2']);

				$date->setDate(date('Y'), date('m'), date('d'));
    			$date->setTime($split_hours[0], $split_hours[1]);
				$date->modify('-'. $days . $days_str);
			}
			
			if ($return_as_obj) 
				return $date;
			return $date->format('Y-m-d h:i:s');
		}

		/**
		 * Handles sending password retrieval email to user.
		 *
		 */
		private function _s2emails_reset_password_url($user_email) {
			if ( empty($user_email) ) {
				return '';
			}
			// retrieve user object based on email address
			$user_data = get_user_by( 'email', trim( wp_unslash( $user_email ) ) );

			if ( empty( $user_data ) ) {
				return '';
			}
			// get key token to be sent to user
			$key = get_password_reset_key( $user_data );

			if ( is_wp_error( $key ) ) {
				return '';
			}
			// prepare reset password link to be retrieved and sent
			$reset_passwork_url = network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_data->user_login), 'login');

			return $reset_passwork_url;
		}

		/**
		 * Unsubscribe user
		 */
		private function _unsubscribe_user()
		{
			global $wpdb;

			if ( (!isset($_GET['u']) || empty($_GET['u']))
			    || !is_numeric($_GET['u'])
			    || (!isset($_GET['token'])
			        || empty($_GET['token'])
			        || strlen($_GET['token']) < 16) ) {
				return false;
			}

			$id_user = (int)$_GET['u'];
			$user_unsub_token = get_user_meta( $id_user, $wpdb->prefix.'s2emails_unsubscribe_token', true );
			$token = sanitize_text_field( trim($_GET['token']) );

			if ( $user_unsub_token && $user_unsub_token == $token ) {
				update_user_meta( $id_user, $wpdb->prefix.'s2member_reminders_enable', 0 );
				update_user_meta( $id_user, $wpdb->prefix.'s2emails_unsubscribe_token', null );
			}
		}

		/**
		 * Update the auto emails option
		 */
		public static function updpateAutoEmailsOpts()
		{
			if (isset($_POST['sub_auto_emails_opt']) && isset($_POST['action']) && $_POST['action'] === 'update_auto_emails_opts') {
				// CSRF checking
				check_admin_referer( 'automated-emails-options' );

				$val = (int)$_POST['s2emails_send_auto_email_to'];
				update_option( 's2emails_send_auto_email_to', $val, false );

        		do_action( 's2emails_admin_auto_mail_opts', $val );
			}
		}

		public function checkAutoEmailsOptions($send_mail_mode)
		{
			// if not set or mode set to only new members
			if ( !(int)$send_mail_mode || (int)$send_mail_mode == 1 ) {
				S2emailRepository::updateStaticPendingMails(array('status' => 1), array('status' => 0));
			} else if ((int)$send_mail_mode == 2) {
				// prepare pending emails for users
				$templates = $this->s2emails_repo->getTemplateMails(1, 1, array(), ARRAY_A);

				if (count($templates))
				foreach ($templates as $template) {
					$template['roles'] = explode('|', $template['roles']);
					$this->checkNewPersistedMail((int)$template['id'], $template, true, 2);

				}
			}
		}

		/**
		 * Update add extre time to users EOT option
		 */
		public static function updpateExtraTimeEotOpt()
		{
			if (isset($_POST['sub_extra_time_eot_opt']) && isset($_POST['action']) && $_POST['action'] === 'update_extra_time_eot_opt') {
				// CSRF checking
				check_admin_referer( 'extra-time-eot-option' );

				$val = $_POST['s2emails_extra_time_eot'];
				update_option( 's2emails_extra_time_eot', $val, false );

        		do_action( 's2emails_extra_time_eot_opt', $val );
			}
		}

		public function checkExtraTimeEotOption($extra_time)
		{
			// get all after/before exp pending emails
			$results = $this->s2emails_repo->getBeAfExpPendingMails(0, '"before_exp", "after_exp"');

			// no results! just stop executing
			if (!count($results)) {
				return;
			}

			// convert extra time to be added to the user's EOT
			$getExtraTime = self::convertGivenExtraTime($extra_time);

			if (false === $getExtraTime) {
				return;
			}

			foreach ($results as $result) {
				if (empty($result->user_eot) || empty($result->send_mail_at)) {
					continue;
				}

				$send_mail_at = (strtotime($result->send_mail_at) + $getExtraTime);

				S2emailRepository::updateStaticPendingMails(array('send_mail_at' => date('Y-m-d H:i:s', $send_mail_at)), array('id' => $result->id));
			}
		}

		public static function getRealTimeOpt($time)
		{
			$matches = preg_split('/(?<=[0-9])(?=[a-z])/i', $time);

			return $matches;
		}

		public static function getRelevantTimeString($time)
		{
			$matches = self::getRealTimeOpt($time);
			$str = '';

			if (!isset($matches[1])) {
				return $str;
			}

			switch ($matches[1]) {
				case 'h':
					$str = ((int)$matches[0] > 1 ? 'hours' : 'hour');
				break;
				case 'd':
					$str = ((int)$matches[0] > 1 ? 'days' : 'day');
				break;
			}

			return $str;
		}

		public static function convertGivenExtraTime($time)
		{
			$matches = self::getRealTimeOpt($time);

			if (!isset($matches[1])) {
				return false;
			}

			$timestamp = 0;
			switch ($matches[1]) {
				case 'h':
					$timestamp = ((int)$matches[0] * 60 * 60);
				break;
				case 'd':
					$timestamp = ((int)$matches[0] * 24 * 60 * 60);
				break;
			}

			return $timestamp;
		}
	}
}