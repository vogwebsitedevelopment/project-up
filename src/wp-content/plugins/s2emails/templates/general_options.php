<div class="wrap">
  <h2><?php _e('General Options','s2emails'); ?></h2><br />
  <form method="post" action="options.php">
      <?php wp_nonce_field('update-options') ?>
    <div class="s2emails_postbox">
        <div class="s2emails_row s2emails_clearfix">
          <div class="s2emails_label">
            <strong><?php _e('Default "From" Name:','s2emails'); ?></strong>
          </div>
          <div class="s2emails_input">
            <input type="text" name="s2emails_from_title" size="45" value="<?php echo get_option('s2emails_from_title'); ?>" />
          </div>
        </div>
        <div class="s2emails_row s2emails_clearfix">
          <div class="s2emails_label">
            <strong><?php _e('Default "From" Email:','s2emails'); ?></strong>
          </div>
          <div class="s2emails_input">
            <input type="text" name="s2emails_from_email" size="45" value="<?php echo get_option('s2emails_from_email'); ?>" />
            <span class="s2emails_info"><?php _e('(For sending the emails you should use a valid email address registered on your domain:','s2emails'); ?> your_email@<strong>your_wordpress_domain.com</strong>)</span>

          </div>
        </div>

        <div class="s2emails_row s2emails_clearfix s2emails_hr"></div>
        
        <div class="s2emails_row s2emails_clearfix">
          <div class="s2emails_label">
            <strong><?php _e('Debug Email Address (optional):','s2emails'); ?></strong>
          </div>
          <div class="s2emails_input">
            <input type="text" name="s2emails_copy_email" size="45" value="<?php echo get_option('s2emails_copy_email'); ?>" /><br />
            <span class="s2emails_info"><?php _e('If you put your email here, you will get copies of all sent emails and also test emails will be sent on this address','s2emails'); ?></span>
          </div>
        </div>

        <div class="s2emails_row s2emails_clearfix s2emails_hr"></div>

        <div class="s2emails_row s2emails_clearfix">
          <div class="s2emails_label">
            <strong><?php _e('Set cron job frequency','s2emails'); ?>:</strong>
          </div>
          <div class="s2emails_input">
          <?php 
            $getS2emailCronJobOption = get_option('s2emails_cron_job_duration');
            $cronJobsDurations       = array(10, 20, 30);
          ?>
            <select name="s2emails_cron_job_duration">
            <?php 
              foreach ($cronJobsDurations as $duration) {
                $selected = ($duration == $getS2emailCronJobOption ? ' selected="selected"' : '');
                echo '<option value="'. $duration .'"'. $selected .'>'. __('Every '. $duration .' minutes','s2emails') .'</option>';
              }
            ?>
            </select><br />
            <span class="s2emails_info"><?php _e('The frequency of how often should s2Emails check if there are emails to be sent','s2emails'); ?></span>
          </div>
        </div>

        <div class="s2emails_row s2emails_clearfix s2emails_hr"></div>

        <div class="s2emails_row s2emails_clearfix">
          <div class="s2emails_label">
            <strong><?php _e('Your timezone','s2emails'); ?>:</strong>
          </div>
          <div class="s2emails_input">
            <select id="s2emails_timezone" name="s2emails_timezone">
              <?php echo S2emailCore::s2emals_timezone(get_option('s2emails_timezone')); ?>
            </select>
          </div>
        </div>

        <div class="s2emails_row s2emails_clearfix">
          <div class="s2emails_label">
            <strong><?php _e('Your local time','s2emails'); ?>:</strong>
          </div>
          <div class="s2emails_input">
            <?php echo date("Y-m-d H:i:s"); ?>
          </div>
        </div>
    </div>      
    <p><input type="submit" name="Submit" class="button button-primary button-large" value="<?php _e('Save Options','s2emails'); ?>" /></p>
    <input type="hidden" name="action" value="update" />
    <input type="hidden" name="page_options" value="s2emails_from_email,s2emails_from_title,s2emails_copy_email,s2emails_cron_job_duration,s2emails_timezone" />
  </form>
</div>

<div class="wrap">
  <h2><?php _e('Recipients','s2emails'); ?></h2><br />
  <form method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
      <?php wp_nonce_field('automated-emails-options') ?>
    <div class="s2emails_postbox">
        <div class="s2emails_row s2emails_clearfix">
          <div class="s2emails_label">
            <strong><?php _e('Send new automated emails to:','s2emails'); ?></strong>
          </div>
          <div class="s2emails_input">
            <?php 
                $opt1 = (1 == get_option('s2emails_send_auto_email_to') ? ' checked="checked"': '');
                $opt2 = (2 == get_option('s2emails_send_auto_email_to') ? ' checked="checked"': '');
            ?>
            <input type="radio" name="s2emails_send_auto_email_to" value="1"<?php echo $opt1; ?> /> <?php _e('Only to new members', 's2emails') ?><br />
            <input type="radio" name="s2emails_send_auto_email_to" value="2"<?php echo $opt2; ?> /> <?php _e('To new and already registered members', 's2emails') ?><br />
            <span class="s2emails_info"><?php _e('s2Emails will scan all your current members and create appropriate emails for them if they fit the conditions of your automated emails','s2emails'); ?></span>
          </div>
        </div>
    </div>      
    <p><input type="submit" name="sub_auto_emails_opt" class="button button-primary button-large" value="<?php _e('Save options','s2emails'); ?>" />
    <input type="hidden" name="action" value="update_auto_emails_opts" /></p>
  </form>
</div>

<div class="wrap">
  <h2><?php _e('s2Emails: Add extra time for EOT','s2emails'); ?></h2><br />
  <form method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
      <?php wp_nonce_field('extra-time-eot-option') ?>
    <div class="s2emails_postbox">
        <div class="s2emails_row s2emails_clearfix">
          <div class="s2emails_label">
            <strong><?php _e('Add extra hour/day(s) time to EOT:','s2emails'); ?></strong>
          </div>
          <div class="s2emails_input">
            <select class="extra_time_eot" name="s2emails_extra_time_eot">
              <?php 
                  $extra_time = get_option('s2emails_extra_time_eot');
                  $available_time = array('1h','2h','3h','4h','5h','6h','7h','8h','9h','10h','11h','12h','1d','2d');

                  foreach ($available_time as $val) {
                    $realVal = str_replace(array('h', 'd'), '', $val);
                    $selected = ($extra_time === $val ? ' selected="selected"' : '');
                    echo '<option value="'. $val .'"'. $selected .'>'. $realVal .' '. S2emailCore::getRelevantTimeString($val) .'</option>';
                  }
              ?>
            </select><br />
            <span class="s2emails_info"><?php _e('s2Emails will scan all your current members and create appropriate emails for them if they fit the conditions of your automated emails','s2emails'); ?></span>
          </div>
        </div>
    </div>      
    <p><input type="submit" name="sub_extra_time_eot_opt" class="button button-primary button-large" value="<?php _e('Update','s2emails'); ?>" />
    <input type="hidden" name="action" value="update_extra_time_eot_opt" /></p>
  </form>
</div>