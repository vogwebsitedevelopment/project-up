<?php
global $wpdb;
$table = $wpdb->prefix.'s2emails';

if(isset($_POST['action']) && $_POST['action']=='addemail') {
    $type_of_schedule = (int)$_POST['type_of_schedule'];

    // check for roles
    if ( !count($_POST['roles']) ) {
        wp_redirect(admin_url().'admin.php?page=s2emails-new-automated-email');
        exit();
    }

	if ($_POST['time_type']==1) {
        if(($type_of_schedule == 2) && ($_POST['days'] == 0) && ($_POST['hours'] == 0)) {
            $_POST['hours'] = 1;
        }
		$day = $_POST['days'];
		$hour = $_POST['hours'];
		$day_2 = '';
		$hours_2 = '';
	} else {
		$day = '';
		$hour = '';
		$day_2 = $_POST['days_2'];
		$hours_2 = $_POST['hours_2'];
	}

    $role = array();
    foreach ($_POST['roles'] as $value) {
        if (isset($_POST[$value.'_ccaps']) && $_POST[$value.'_ccaps'] != '' && $type_of_schedule != 5) {
            $role[] = $value.':'. str_replace(' ', '', $_POST[$value.'_ccaps']);
        } else {
            $role[] = $value;
        }
    }

    $get_new_roles = '';
    $new_roles = (isset($_POST['new_roles']) ? $_POST['new_roles'] : array());
    $cc_emails = null;
   
    if (isset($_POST['cc_emails']) && !empty($_POST['cc_emails'])) {
        $_emails = explode(',', trim($_POST['cc_emails']));
        $_ems = array();

        if (count($_emails))
        foreach ($_emails as $_email) {
            $_email = trim($_email);
            if (filter_var($_email, FILTER_VALIDATE_EMAIL)) {
                $_ems[] = $_email;
            }
        }

        if (count($_ems)) {
            $cc_emails = implode(',', $_ems);
        }
    }

    if ($type_of_schedule == 5) {
        if (!count($new_roles)) {
            wp_redirect(admin_url().'admin.php?page=s2emails-new-automated-email');
            exit();
        } else {
            $get_new_roles = implode('#', $new_roles);
        }
    }

    $roles = join('|', $role);

    if (isset($_POST['id']) && !empty($_POST['id'])) {
        $sql = "UPDATE `".$table."` SET title = '".$_POST['title']."' , subject = '".$_POST['subject']."' , time_type = '".$_POST['time_type']."' , type_of_schedule = '".$type_of_schedule."' , days = '".$day."' , hours = '".$hour."'  , days_2 = '".$day_2."' , hours_2 = '".$hours_2."' , content_type = '".$_POST['content_type']."' , active = '".$_POST['active']."' , content = '".$_POST['content']."', update_at = '".date('Y-m-d h:i:s')."', roles = '".$roles."', new_roles = '". $get_new_roles ."', cc_emails = '". $cc_emails ."'";
        $sql .= " WHERE id = '". (int) $_POST['id']."'";

        $wpdb->query($sql);

        do_action('s2emails_admin_updated_auto_mail', (int)$_POST['id'], $_POST);

        wp_redirect(admin_url().'admin.php?page=s2emails&act=edit');
        exit();
    } else {
        // do_action( 's2emails_admin_inserted_auto_mail_before', 0, $_POST);

        $wpdb->insert($table,
            array(
                'title' => trim($_POST['title']), 
                'subject' => trim($_POST['subject']), 
                'time_type' => $_POST['time_type'], 
                'type_of_schedule' => $type_of_schedule, 
                'days' => $day, 
                'hours' => $hour, 
                'days_2' => $day_2, 
                'hours_2' => $hours_2, 
                'content_type' => $_POST['content_type'], 
                'active' => $_POST['active'], 
                'content' => trim($_POST['content']), 
                'create_at' => date('Y-m-d h:i:s'), 
                'update_at' => date('Y-m-d h:i:s'), 
                'roles' => $roles, 
                'new_roles' => $get_new_roles, 
                'cc_emails' => $cc_emails 
            )
        );

        do_action( 's2emails_admin_inserted_auto_mail', $wpdb->insert_id, $_POST);

        wp_redirect(admin_url().'admin.php?page=s2emails&act=new');
        exit();
    }
}

$content = '';
$edit = FALSE;

if (isset($_REQUEST['act']) && $_REQUEST['act']=='edit') {
	$edit = TRUE;
	$getRow = $wpdb->get_row("SELECT * FROM `".$table."` WHERE id = '".$_REQUEST['id']."'");
	$content = $getRow->content;
    $id_mail = $getRow->id;
} else {
    $getTemplate = S2emailRepository::getLastTemplateMail();
    if(is_null($getTemplate)){
        $id_mail = 1;
    }else{
        $id_mail = ($getTemplate->id+1);
    }
}

?>
<div class="wrap">
    <h2><?php
        if($edit== TRUE)
        { ?>
            <?php _e('Update an Automated Email','s2emails'); ?><a class="add-new-h2" href="?page=s2emails-new-automated-email"><?php _e('New Automated Email','s2emails'); ?></a><?php
        }
        else
        {   ?>
            <?php _e('Add a New Automated Email','s2emails'); ?><?php
        }   ?>
    </h2>
    <br />


    <div class="s2emails_postbox">
        <form method="post" id="add_new_email" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" name="post">
            <div id="poststuff">
                <div class="metabox-holder" id="post-body">
                    <div id="post-body-content" style="position: relative;">
                        <div class="s2emails_row s2emails_clearfix">
                            <div class="s2emails_label">
                               	<label class="s2emails_label_text"><?php _e('Email Title:','s2emails'); ?></label>
                            </div>
                            <div class="s2emails_input">
                                <input type="text" autocomplete="off" class="validate" value="<?php if($edit){ echo esc_attr($getRow->title); } ?>" size="100%" name="title" /><br />
                                <span class="s2emails_info"><?php _e('This name will be used for easier identification within s2emails plugin','s2emails'); ?>
                                </span>
                            </div>
                        </div>
                        <div class="s2emails_row s2emails_clearfix">
                            <div class="s2emails_label">
                                <label class="s2emails_label_text"><?php _e('Email Subject:','s2emails'); ?></label>
                            </div>
                            <div class="s2emails_input">
                                <input type="text" autocomplete="off" class="validate" size="100%" value="<?php if($edit){ echo esc_attr($getRow->subject); } ?>" name="subject">
                                <span class="s2emails_info"><?php _e('The subject of the emails that your users will receive','s2emails'); ?>
                            </div>
                        </div>
                        <div class="s2emails_row s2emails_clearfix">
                            <div class="s2emails_label">
                                <label class="s2emails_label_text"><?php _e('CC Emails:','s2emails'); ?></label>
                            </div>
                            <div class="s2emails_input">
                                <input type="text" class="" value="<?php if($edit){ echo esc_attr($getRow->cc_emails); } ?>" size="100%" name="cc_emails" /><br />
                                <span class="s2emails_info"><?php _e('Leave empty or define email(s) that should receive a secret copy of this s2email. Use "," to separate these CC emails','s2emails'); ?>
                                </span>
                            </div>
                        </div>
                        <div class="s2emails_row s2emails_clearfix">
                            <div class="s2emails_label s2emails_radiooffset">
                                    <label class="s2emails_label_text main"><?php _e('When to Send:','s2emails'); ?></label>
                            </div>
                            <div class="s2emails_input">
                                    <?php 
                                    $checked_af = $checked_bf = $checked_exp_af = $checked_shortcode = $text = $checked_userrole = $checked_after_cancel = '';
                                    
                                    if($edit && !empty($getRow->type_of_schedule)) {
                                        if ($getRow->type_of_schedule==1) { 
                                            $text = 'AFTER registration'; 
                                            $checked_af = 'checked=checked'; 
                                        } elseif ($getRow->type_of_schedule==2) {
                                            $text = 'BEFORE expiration';
                                            $checked_bf = 'checked=checked';
                                        } elseif ($getRow->type_of_schedule==3) {
                                            $text = 'AFTER expiration';
                                            $checked_exp_af = 'checked=checked';
                                        } elseif ($getRow->type_of_schedule==4) {
                                            $text = 'After visiting a shortcode';
                                            $checked_shortcode = 'checked=checked';
                                        } elseif ($getRow->type_of_schedule==5) {
                                            $text = 'After changing the user\'s role';
                                            $checked_userrole = 'checked=checked';
                                        } elseif ($getRow->type_of_schedule==6) {
                                            $text = 'After cancellation';
                                            $checked_after_cancel = 'checked=checked';
                                        } 
                                    }
                                    
                                    if($edit && !isset($getRow->type_of_schedule) || empty($getRow->type_of_schedule)){
                                        $text       = 'AFTER registration';
                                        $checked_af = 'checked=checked';
                                    }
                                    
                                    $checked_ta= '';
                                    $checked_tb = '';
                                    if(!empty($getRow->time_type) && $getRow->time_type=='1'){ $display_a = "display:inline-block"; $display_b= "display:none"; $checked_ta = 'checked=checked'; }
                                    if(!empty($getRow->time_type) && $getRow->time_type=='2'){ $display_b = "display:inline-block"; $display_a= "display:none"; $checked_tb = 'checked=checked'; } 
                                    if(!isset($getRow->time_type) || empty($getRow->time_type)){ $display_a = "display:inline-block"; $display_b= "display:none"; $checked_ta = 'checked=checked'; }
                                    ?>
                                     
                                    <input type="radio" class="radio call_this" autocomplete="off" spellcheck="true"  size="30" value="1" <?php echo $checked_af; ?> name="type_of_schedule" id="type_of_schedule_1">
                                    <label class="s2emails_label_text" for="type_of_schedule_1"><?php _e('After registration','s2emails'); ?></label><br />
                                    <input type="radio" class="radio call_this" autocomplete="off" spellcheck="true"  size="30" value="2" <?php echo $checked_bf; ?> name="type_of_schedule" id="type_of_schedule_2">
                                    <label class="s2emails_label_text" for="type_of_schedule_2"><?php _e('Before expiration','s2emails'); ?></label><br />
                                    <input type="radio" class="radio call_this" autocomplete="off" spellcheck="true" size="30" value="3" <?php echo $checked_exp_af; ?> name="type_of_schedule" id="type_of_schedule_3">
                                    <label class="s2emails_label_text" for="type_of_schedule_3"><?php _e('After expiration','s2emails'); ?></label><br />
                                    <input type="radio" class="radio call_this" autocomplete="off" spellcheck="true" size="30" value="4" <?php echo $checked_shortcode; ?> name="type_of_schedule" id="type_of_schedule_4">
                                    <label class="s2emails_label_text" for="type_of_schedule_4"><?php _e('After visiting a shortcode on a page/post','s2emails'); ?> <span class="s2email_code">[s2email_shortcode id_mail="<?php echo $id_mail; ?>"]</span></label>

                                    <br />
                                    <input type="radio" class="radio call_this" autocomplete="off" spellcheck="true" size="30" value="5" <?php echo $checked_userrole; ?> name="type_of_schedule" id="type_of_schedule_5">
                                    <label class="s2emails_label_text" for="type_of_schedule_5"><?php _e('After changing the user\'s role','s2emails'); ?></label>

                                    <br />
                                    <input type="radio" class="radio call_this" autocomplete="off" spellcheck="true" size="30" value="6" <?php echo $checked_after_cancel; ?> name="type_of_schedule" id="type_of_schedule_6">
                                    <label class="s2emails_label_text" for="type_of_schedule_6"><?php _e('After cancellation','s2emails'); ?></label>
                            </div>
                        </div>
                        <div class="s2emails_row s2emails_clearfix">
                            <div class="s2emails_label s2emails_radiooffset">
    								<label class="s2emails_label_text main"><?php _e('At What Time:','s2emails'); ?></label>
                            </div>
                            <div class="s2emails_input">
                                    <input type="radio" class="radio call_time" id="option_time_1" autocomplete="off" spellcheck="true"  size="30" value="1" <?php echo $checked_ta; ?> name="time_type">
                                    <label class="s2emails_label_text" for="option_time_1"><?php _e('Define certain amount of days and hours','s2emails'); ?></label><br />
                                    <input type="radio" class="radio call_time" id="option_time_2" autocomplete="off" spellcheck="true"  size="30" value="2" <?php echo $checked_tb; ?> name="time_type">
                                    <label class="s2emails_label_text" for="option_time_2"><?php _e('Define certain amount of days and certain hour','s2emails'); ?></label>
                            </div>
                        </div>
                        <div class="s2emails_row s2emails_clearfix s2_radiobox_row s2emails_roles_row">
                            <div class="s2emails_label">
                                   <label class="s2emails_label_text main"><?php _e('Send for Roles:', 's2emails'); ?></label>
                            </div>
                            <div class="s2emails_input">
                                    <?php 
                                    $rolesMain = array();
                                    $newRoles = array();
                                    $rolesCcap = array();
                                    if ($edit && !empty($getRow->roles)) {
                                        $roles_explode = explode('|', $getRow->roles);
                                        foreach ($roles_explode as $value) {
                                            $role_detail = explode(':', $value);
                                            $rolesMain[] = $role_detail[0];
                                            if(array_key_exists(1,$role_detail)){
                                                $rolesCcap[$role_detail[0]] = $role_detail[1];
                                            }
                                        }
                                    }

                                    if ($edit && !empty($getRow->new_roles)) {
                                        $newRoles = explode('#', $getRow->new_roles);
                                    }
                                    $is_new_roles_hidden = (empty($checked_userrole) ? ' user_role_hid' : '');
                                    $is_ccap_hidden = (empty($checked_userrole) ? '' : ' style="display:none;"');
                                    ?>
                                    <div class="user_role_labels<?= $is_new_roles_hidden; ?>">
                                      <div class="left_role_side"><?php _e('Old role', 's2emails'); ?></div>
                                      <div class="right_role_side"><?php _e('New role:', 's2emails'); ?></div>
                                    </div>
                                    <?php
                                    $roles = get_editable_roles();
                                    foreach ($roles as $key => $value):
                                        if(in_array($key, array('administrator', 'editor', 'author', 'contributor')))
                                            continue;
                                    ?>
                                    <div class="s2emails_role">
                                        <input class="tpl_roles_ccaps" id="role_<?php echo $key; ?>" type="checkbox"<?php if($edit && is_array($rolesMain) && in_array($key, $rolesMain )): ?> checked="checked"<?php endif; ?> name="roles[]" value="<?php echo $key; ?>" />
                                        <label class="s2emails_label_text" for="role_<?php echo $key; ?>"><?php echo $value['name']; ?></label>
                                        <div class="s2emails_ccaps"<?= $is_ccap_hidden; ?>>
                                        CCAPs (optional)
                                        <input type="input" name="<?php echo $key; ?>_ccaps" <?php echo (isset($rolesCcap[$key]) ? ' value="'. $rolesCcap[$key] .'"' : ' disabled="disabled"'); ?> data-role="<?php echo $key; ?>" />
                                        </div>
                                        <div class="user_singl_role_row<?= $is_new_roles_hidden; ?>">
                                            <input class="new_role_checkbox" id="new_role_<?php echo $key; ?>" type="checkbox"<?php if($edit && in_array($key, $newRoles)): ?> checked="checked"<?php endif; ?> name="new_roles[]" value="<?php echo $key; ?>" />
                                            <label class="s2emails_label_text" for="new_role_<?php echo $key; ?>"><?php echo $value['name']; ?></label>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="s2emails_row s2emails_clearfix" id="timesbox">
                            <div class="s2emails_maininfo">
                                <div class="after_reg" id="one_way_div" style=" <?php echo $display_a; ?>">
                                	<label><?php _e('Send this email','s2emails'); ?></label>
                                    <input type="number" min="0" id="days" class="smaller_input numeric" autocomplete="off" spellcheck="true"  size="30" value="<?php if(!empty($getRow->days)){echo $getRow->days;}else{echo '0';} ?>" name="days">
                                    <label class="s2emails_label_text days"><?php _e('day(s)','s2emails'); ?></label>
                                    <label class="s2emails_label_text">and</label>
                                    <input type="number" min="0" max="23" id="hours" class="smaller_input numeric" autocomplete="off" spellcheck="true"  size="30" value="<?php if(!empty($getRow->hours)){echo $getRow->hours;}else{echo '0';} ?>" name="hours">
                                    <label class="s2emails_label_text days"><?php _e('hour(s)','s2emails'); ?></label>
                                    <label class="s2emails_label_text reg_text" id="reg_text"><?php echo $text; ?></label>
                                    <div id="s2emails_info_immediately" class="s2emails_info"><?php _e('This email will be sent immediately after registration','s2emails'); ?></div>
                                </div>
                                <div class="after_reg" id="two_way_div"  style=" <?php echo $display_b; ?>">
                                    <label><?php _e('Send this email','s2emails'); ?></label>
                                    <input type="number" min="0" id="days_2" class="smaller_input numeric" autocomplete="off" spellcheck="true"  size="30" value="<?php if(!empty($getRow->days_2)){echo $getRow->days_2;}else{echo '0';} ?>" name="days_2">
                                    <label class="s2emails_label_text days"><?php _e('days','s2emails'); ?></label>
                                    <label class="s2emails_label_text"><?php _e('at','s2emails'); ?></label>
                                    <select name="hours_2" autocomplete="off">
                                    <?php $hours = array(
                                        '12:00 AM' => '00:00:00',
                                        '1:00 AM' => '01:00:00',
                                        '2:00 AM' => '02:00:00',
                                        '3:00 AM' => '03:00:00',
                                        '4:00 AM' => '04:00:00',
                                        '5:00 AM' => '05:00:00',
                                        '6:00 AM' => '06:00:00',
                                        '7:00 AM' => '07:00:00',
                                        '8:00 AM' => '08:00:00',
                                        '9:00 AM' => '09:00:00',
                                        '10:00 AM' => '10:00:00',
                                        '11:00 AM' => '11:00:00',
                                        '12:00 PM' => '12:00:00',
                                        '1:00 PM' => '13:00:00',
                                        '2:00 PM' => '14:00:00',
                                        '3:00 PM' => '15:00:00', 
                                        '4:00 PM' => '16:00:00', 
                                        '5:00 PM' => '17:00:00', 
                                        '6:00 PM' => '18:00:00', 
                                        '7:00 PM' => '19:00:00', 
                                        '8:00 PM' => '20:00:00',
                                        '9:00 PM' => '21:00:00', 
                                        '10:00 PM' => '22:00:00', 
                                        '11:00 PM' => '23:00:00');

                                    foreach ($hours as $key => $value) {
                                        echo '<option '. ((($edit && $getRow->hours_2 == $value) or (!$edit && $value == '08:00:00')) ? 'selected="selected"': '').' value="'.$value.'">'.$key.'</option>';
                                    }
                                    
                                    ?>
                                    </select>
                                    <label class="s2emails_label_text reg_text" id="reg_text"><?php echo $text; ?></label>
                                    <div id="s2emails_info_time" class="s2emails_info"><?php _e('The email will be sent at the day of registration','s2emails'); ?></div>
                                    <div class="s2emails_info"><em><?php _e('Set the time according to your local timezone. (Set it in s2Emails > General Options)','s2emails'); ?></em></div>
                            </div>
                        </div>
                    </div>
                        <div class="s2emails_row s2emails_clearfix">
                            <div class="s2emails_label s2emails_radiooffset">
                                <?php 
    								$checked_aa = '';
    								$checked_bb = '';
    								if(!empty($getRow->active) && $getRow->active=='1'){ $checked_aa = 'checked=checked'; }
    								if(!empty($getRow->active) && $getRow->active=='2'){ $checked_bb = 'checked=checked'; } 
    								if(!isset($getRow->active) || empty($getRow->active)){ $checked_aa = 'checked=checked'; }?>
                                   <label class="s2emails_label_text main"><?php _e('Activate:','s2emails'); ?></label>
                            </div>
                            <div class="s2emails_input">
                                    <input type="radio" class="radio" id="activate_1" autocomplete="off" spellcheck="true" <?php echo $checked_aa;?> size="30" value="1" name="active">
                                    <label class="s2emails_label_text" for="activate_1"><?php _e('Yes','s2emails'); ?></label><br />
                                    <input type="radio" class="radio" id="activate_2" autocomplete="off" spellcheck="true"  size="30" value="2" <?php echo $checked_bb;?> name="active">
                                    <label class="s2emails_label_text" for="activate_2"><?php _e('No','s2emails'); ?></label>
                            </div>
                        </div>
                        <div class="s2emails_row s2emails_clearfix">
                            <div class="s2emails_label s2emails_radiooffset">
    								<label class="s2emails_label_text main"><?php _e('Content Type:','s2emails'); ?></label>
                            </div>
                            <div class="s2emails_input">
                                    <?php 
    								$checked_dd = '';
    								$checked_cc = '';
    								if(!empty($getRow->content_type) && $getRow->content_type=='1'){ $checked_dd = 'checked=checked'; }
    								if(!empty($getRow->content_type) && $getRow->content_type=='2'){ $checked_cc = 'checked=checked'; } 
    								if(!isset($getRow->content_type) || empty($getRow->content_type)){ $checked_dd = 'checked=checked'; }?>
                                    <input type="radio" class="radio" id="content_1" autocomplete="off" spellcheck="true" <?php echo $checked_dd;?> size="30" value="1" name="content_type">
                                    <label class="s2emails_label_text" for="content_1"><?php _e('Plain/Text','s2emails'); ?></label><br />
                                    <input type="radio" class="radio" id="content_2" autocomplete="off" spellcheck="true"  size="30" value="2" <?php echo $checked_cc;?> name="content_type">
                                    <label class="s2emails_label_text" for="content_2"><?php _e('Text/Html','s2emails'); ?></label>
                                
                            </div>
                        </div>
                        <div class="s2emails_row s2emails_clearfix">
                            <div class="s2emails_label">
                                <?php _e('Shortcodes:','s2emails'); ?> 
                            </div>
                            <div class="s2emails_input">   
                                <strong>{first_name}, {last_name}, {display_name}, {username}, {date_of_registration}, {date_of_expiration}, {user_role}, {new_password}, {unsubscribe}...{/unsubscribe}</strong>
                            </div>
                        </div>
                        <div class="s2emails_row s2emails_clearfix">
                            <div class="postarea wp-editor-expand" id="postdivrich">
                                <?php 
                                wp_editor( $content, 'content', $settings = array() );
                                ?>
                            </div>
                        </div>
                        <div class="submit-btns">
                            <?php 
    						if(isset($_REQUEST['id']) && !empty($_REQUEST['id'])):
    							echo '<input type="hidden" name="id" value="'.$getRow->id.'">';
                            endif;
                            ?>
                        	<input type="hidden" value="addemail" name="action">
                        	<input type="submit" value="<?php _e('Submit','s2emails'); ?>" class="button button-primary button-large" >
                        	<a href="?page=s2emails"><input type="button" class="button cancel_btn" value="<?php _e('Cancel','s2emails'); ?>" name="save"></a>
                       	</div>
                    </div>
                </div>
                <br class="clear">
            </div>
        </form>
        <br class="clear">
    </div>
</div>

<script>
// Define messages
var afterRegMsg = "<?php _e('This email will be sent immediately after registration','s2emails'); ?>",
    afterRegMsgInfo = "<?php _e('The email will be sent at the day of registration','s2emails'); ?>",
    afterExpMsg = "<?php _e('This email will be sent one hour after expiration','s2emails'); ?>",
    afterExpMsgInfo = "<?php _e('The email will be sent at the day of expiration','s2emails'); ?>",
    beforeExpMsg = "<?php _e('This email will be sent within the last hour before expiration','s2emails'); ?>",
    beforeExpMsgInfo = "<?php _e('The email will be sent at the day of expiration','s2emails'); ?>",
    shortcodeMsg = "<?php _e('This email will be sent immediately after visiting the shortcode','s2emails'); ?>",
    shortcodeMsgInfo = "<?php _e('This email will be sent immediately after visiting the shortcode','s2emails'); ?>",
    userRoleMsg = "<?php _e('This email will be sent immediately after changing the user\'s role','s2emails'); ?>",
    userRoleMsgInfo = "<?php _e('This email will be sent immediately after changing the user\'s role','s2emails'); ?>",
    afterCancellationMsg = "<?php _e('This email will be sent immediately after cancellation','s2emails'); ?>",
    afterCancellationMsgInfo = "<?php _e('This email will be sent immediately after cancellation','s2emails'); ?>";
(function($) {
$(document).ready(function(){
    $("#days, #hours, #days_2, .call_this").on("change paste keyup", function() {
        var getVal = parseInt($(this).val()),
            scheduleType = parseInt($('input[name=type_of_schedule]:checked').val());
        
        setGeneralDaysHoursMessages(scheduleType, $("#days"), $("#hours"));
        setCertainDaysHoursMessages(scheduleType, $("#days_2"));
    });

    $(".call_this").on("change paste keyup", function() {
        var getVal = parseInt($(this).val());

        if (getVal == 1) {
            $(".reg_text").html("<?php _e('AFTER registration','s2emails'); ?>");
            $('.s2emails_roles_row .user_role_labels, .user_singl_role_row').addClass('user_role_hid');
            $('.s2emails_roles_row .s2emails_ccaps').show();
        } else if (getVal == 2) {
            $(".reg_text").html("<?php _e('BEFORE expiration','s2emails'); ?>");
            // check if first time option was selected for before expiration 
            if (parseInt($("input[name=time_type]:checked").val()) == 1) {
                catchBeforeExpNoTimeMessage();
            }
            $('.s2emails_roles_row .user_role_labels, .user_singl_role_row').addClass('user_role_hid');
            $('.s2emails_roles_row .s2emails_ccaps').show();
        } else if (getVal == 3) {
            $(".reg_text").html("<?php _e('AFTER expiration','s2emails'); ?>");
            $('.s2emails_roles_row .user_role_labels, .user_singl_role_row').addClass('user_role_hid');
            $('.s2emails_roles_row .s2emails_ccaps').show();
        } else if (getVal == 4) {
            $(".reg_text").html("<?php _e('AFTER visiting a shortcode','s2emails'); ?>");
            $('.s2emails_roles_row .user_role_labels, .user_singl_role_row').addClass('user_role_hid');
            $('.s2emails_roles_row .s2emails_ccaps').show();
        } else if (getVal == 5) {
            $(".reg_text").html("<?php _e('AFTER changing the user\'s role','s2emails'); ?>");
            $('.s2emails_roles_row .s2emails_ccaps').hide();
            $('.s2emails_roles_row .user_role_labels, .user_singl_role_row').removeClass('user_role_hid');
        } else if (getVal == 6) {
            $(".reg_text").html("<?php _e('AFTER cancellation','s2emails'); ?>");
            $('.s2emails_roles_row .user_role_labels, .user_singl_role_row').addClass('user_role_hid');
            $('.s2emails_roles_row .s2emails_ccaps').show();
        }
    });

    var getCheckedScheduleType = parseInt($('input[name=type_of_schedule]:checked').val()); 
    setGeneralDaysHoursMessages(getCheckedScheduleType, $("#days"), $("#hours"));
    setCertainDaysHoursMessages(getCheckedScheduleType, $("#days_2"));

    $(".call_time").click(function() {
        if(this.value==1) {
            $("#two_way_div").fadeOut('fast',function(){ $("#one_way_div").fadeIn("fast"); });
            if (parseInt($("input[name=type_of_schedule]:checked").val()) == 2) {
                catchBeforeExpNoTimeMessage();
            }
        } else {
            $("#one_way_div").fadeOut('fast',function(){ $("#two_way_div").fadeIn("fast"); });
        }
    });

    $("#add_new_email").submit(function() {
        error = 0;
        $("#add_new_email .validate").each(function() {
            if(this.value=='') {
                $(this).addClass("error");
                error++;
            } else {
                $(this).removeClass("error");
            }
        });

        $("#add_new_email .error").each(function() {
            $('html, body').animate({
                scrollTop: $(this).offset().top-60
            }, 1500);

            return false;
        });

        var is_role_checked = false;
        $(".tpl_roles_ccaps").each(function() {
            if ( $(this).is(':checked') ) {
                is_role_checked = true;
            }
        });

        if (is_role_checked == false) {
            if (parseInt($('.call_this:checked').val()) == 5) {
                alert('You need to check at least one old role and one new role');
            } else {
                alert('You need to check at least one role');
            }

            $('html,body').animate({
                scrollTop: $(".s2emails_roles_row").offset().top
            }, 'slow');

            $('.tpl_roles_ccaps, .new_role_checkbox').addClass("error");
            error++;
        } else {
            $('.tpl_roles_ccaps, .new_role_checkbox').removeClass("error");
        }

        if (error == 0) {
            return true;
        }
        return false;
    });

    // enable/disable ccaps
    $(".tpl_roles_ccaps").on('change', function() {
        var getRole = $(this).val();

        if ($(this).is(':checked')) {
            $(this).parent().find(".s2emails_ccaps > input").attr('disabled', false);
        } else {
            $(this).parent().find(".s2emails_ccaps > input").attr('disabled', true).val("");
        }
    });
});
function catchBeforeExpNoTimeMessage() {
    if (parseInt($("#days").val()) == 0 && parseInt($("#hours").val()) == 0) {
        // alert("<?php _e('You need to set at least 1 hour before expiration','s2emails'); ?>");
        $("#hours").val(1);
    }
}
function setGeneralDaysHoursMessages(val, days_obj, hours_obj) {
    if(parseInt(days_obj.val()) != 0 || parseInt(hours_obj.val()) != 0) {
        $("#s2emails_info_immediately").html("");
        return false;
    }
 
    if (val == 1) {
        $("#s2emails_info_immediately").html(afterRegMsg);
    } else if (val == 2) {
        $("#s2emails_info_immediately").html(beforeExpMsg);
        if (parseInt($("input[name=time_type]:checked").val()) == 1) {
            catchBeforeExpNoTimeMessage();
        }
    } else if (val == 3) {
        $("#s2emails_info_immediately").html(afterExpMsg);
    } else if (val == 4) {
        $("#s2emails_info_immediately").html(shortcodeMsg);
    } else if (val == 5) {
        $("#s2emails_info_immediately").html(userRoleMsg);
    } else if (val == 6) {
        $("#s2emails_info_immediately").html(afterCancellationMsg);
    }
}
function setCertainDaysHoursMessages(val, days_2) {
    if (parseInt(days_2.val()) != 0) {
        $("#s2emails_info_time").html("");
        return false;
    }
 
    if (val == 1) {
        $("#s2emails_info_time").html(afterRegMsgInfo);
    } else if (val == 2) {
        $("#s2emails_info_time").html(beforeExpMsgInfo);
    } else if (val == 3) {
        $("#s2emails_info_time").html(afterExpMsgInfo);
    } else if (val == 4) {
        $("#s2emails_info_time").html(shortcodeMsgInfo);
    } else if (val == 5) {
        $("#s2emails_info_time").html(userRoleMsgInfo);
    } else if (val == 6) {
        $("#s2emails_info_time").html(afterCancellationMsgInfo);
    }
}
})(jQuery);
</script>