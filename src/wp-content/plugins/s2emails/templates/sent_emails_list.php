<?php
global $wpdb;
$table = $wpdb->prefix.'s2emails_sent';
$page = (isset($_REQUEST['p']) && !empty($_REQUEST['p'])) ? $_REQUEST['p'] : 1;
$cur_page = $page;
$page -= 1;
$per_page = 30;
$previous_btn = true;
$next_btn = true;
$first_btn = true;
$last_btn = true;
$start = $page * $per_page;
$activePublish = '';


if(isset($_REQUEST['type']) && $_REQUEST['type']=='trash')
{
	$condition = "WHERE status = 2";
}else
{
	$condition = "WHERE status = 1";
}
$activeAll = (isset($_REQUEST['type']) && $_REQUEST['type']=='all')? 'current' : '';
$activeTrash = (isset($_REQUEST['type']) && $_REQUEST['type']=='trash')? 'current' : '';
$activePublish = ((isset($_REQUEST['type']) && $_REQUEST['type']=='publish') || !isset($_REQUEST['type']))? 'current' : '';
	
$listData =$wpdb->get_results("SELECT * from `".$table."` ".$condition." ORDER BY sent_time DESC LIMIT $start, $per_page");
$wpdb->get_results("SELECT * FROM `".$table."` ".$condition."");
$query_pag_num = $wpdb->num_rows;

$wpdb->get_results("SELECT * FROM `".$table."` WHERE status = 1 ");
$publish = $wpdb->num_rows;

$wpdb->get_results("SELECT * FROM `".$table."` WHERE status = 2 ");
$trash = $wpdb->num_rows;

if(isset($_REQUEST['type']) && $_REQUEST['type']=='trash')
{
	$url = admin_url().'admin.php?page=s2emails-list-sent-emails&type=trash';
}
elseif(isset($_REQUEST['type']) && $_REQUEST['type']=='publish')
{
	$url = admin_url().'admin.php?page=s2emails-list-sent-emails&type=publish';
}
elseif(isset($_REQUEST['type']) && $_REQUEST['type']=='all')
{
	$url = admin_url().'admin.php?page=s2emails-list-sent-emails&type=all';
}
else
{
	$url = admin_url().'admin.php?page=s2emails-list-sent-emails';
} 


?> 
<div class="wrap">
    <h2><?php _e('List of Sent Emails'); ?></h2><br />
    <form method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" id="posts-filter">
        <table class="wp-list-table widefat fixed striped pages">
            <thead>
            <tr>
                <th class="manage-column column-title sortable desc" id="title" scope="col" style="padding-left:8px;"><?php _e('Email Address','s2emails'); ?></th>
                <th style="" class="manage-column column-title" id="title" scope="col"><?php _e('Automated Email','s2emails'); ?></th>
                <th style="" class="manage-column column-tags" id="title" scope="col"><?php _e('Your Local Time','s2emails'); ?></th>
                <th style="" class="manage-column column-tags" id="title" scope="col"><?php _e('Action','s2emails'); ?></th>
            </tr>
            </thead>
            <tbody id="the-list">
                <?php
				if($query_pag_num > 0) {
					foreach($listData as $data) { 
						?>
						<tr class="iedit author-self level-0 post-7 type-page status-publish hentry" id="post-7">
							
							<td class="post-title page-title column-title">
                            	<a title="<?php _e('View User','s2emails'); ?>" href="<?php echo get_edit_user_link($data->user_id); ?>"><?php echo $data->email; ?></a><br />
                                <div class="row-actions">
                                    <span class="edit"><a title="<?php _e('View User','s2emails'); ?>" href="<?php echo get_edit_user_link($data->user_id); ?>"><?php _e('View User','s2emails'); ?></a></span> 
                                </div>
							</td>			
							<td class="post-title page-title column-title">
                                <?php echo $data->title; ?>
                          	</td>
                            <td class="date column-tags">
                            <?php
                                $sentTime = new DateTime($data->sent_time);
                                echo $sentTime->format('Y-m-d g:iA');
                            ?>
                          	</td>
                            <td class="date column-tags ">
                               <a href="?page=s2emails-new-automated-email&act=edit&id=<?php echo $data->mail_id; ?>" class="view_cont" title="<?php _e('Edit Automated Email','s2emails'); ?>"><?php _e('Edit Automated Email','s2emails'); ?></a></a>
                          	</td>
						</tr><?php
					}
				}
				else
				{
					echo '<tr class="no-items"><td colspan="4" class="colspanchange">'. __('No sent emails yet','s2emails') .'</td></tr>';
				}
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <th style="padding-left:8px;" class="manage-column column-title sortable desc" id="title" scope="col"><?php _e('Email address','s2emails'); ?></th>
                    <th style="" class="manage-column column-title" id="title" scope="col"><?php _e('Automated email','s2emails'); ?></th>
                    <th style="" class="manage-column column-tags" id="title" scope="col"><?php _e('Time','s2emails'); ?></th>
                    <th style="" class="manage-column column-tags" id="title" scope="col"><?php _e('Action','s2emails'); ?></th>
                </tr>
            </tfoot>
        </table>
        <div class="tablenav bottom">
            <div class="alignleft actions"></div>
           	<?php include(S2EMAILS_TPL_DIR .'_partials/pagination.php'); ?>
        </div>
    </form>
    <div id="ajax-response"></div>
    <br class="clear">
</div>
