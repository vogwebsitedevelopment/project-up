<?php
	$count = $query_pag_num;
	$no_of_paginations = ceil($count / $per_page);

	if ($cur_page >= 7) {
		$start_loop = $cur_page - 3;
		if ($no_of_paginations > $cur_page + 3)
			$end_loop = $cur_page + 3;
		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
			$start_loop = $no_of_paginations - 6;
			$end_loop = $no_of_paginations;
		} else {
			$end_loop = $no_of_paginations;
		}
	} else {
		$start_loop = 1;
		if ($no_of_paginations > 7)
			$end_loop = 7;
		else
			$end_loop = $no_of_paginations;
	}
$msg = '<div class="tablenav-pages">
	<span class="displaying-num">'.$query_pag_num.'  '.__('items','s2emails').'</span>';
	

	if ($first_btn && $cur_page > 1) {
		$msg .= '<a href="'.$url.'" title="'.__('Go to the first page','s2emails').'" class="first-page">«</a>';
	} else if ($first_btn) {
		$msg .= '<a href="javascript:;" title="'.__('Go to the first page','s2emails').'" class="first-page disabled">«</a>';
	}

	if ($previous_btn && $cur_page > 1) {
		$pre = $cur_page - 1;
		$msg .= '<a href="'.$url.'&p='.$pre.'" title="'.__('Go to the previous page','s2emails').'" class="prev-page">‹</a>';
	} else if ($previous_btn) {
		$msg .= '<a href="javascript:;" title="'.__('Go to the previous page','s2emails').'" class="prev-page disabled">‹</a>';
	}
	$msg .= '<span class="paging-input">'.$cur_page.' of <span class="total-pages">'.$no_of_paginations.' </span></span>';


	if ($next_btn && $cur_page < $no_of_paginations) {
		$nex = $cur_page + 1;
		$msg .= '<a href="'.$url.'&p='.$nex.'" title="'.__('Go to the next page','s2emails').'" class="next-page">›</a>';
	} else if ($next_btn){
		$msg .= '<a href="javascript:;" title="'.__('Go to the next page','s2emails').'" class="next-page disabled">›</a>';
	}

	if ($last_btn && $cur_page < $no_of_paginations) {
		$msg .= '<a href="'.$url.'&p='.$no_of_paginations.'" title="'.__('Go to the last page','s2emails').'" class="last-page">»</a>';
	} else if ($last_btn) {
		$msg .= '<a href="javascript:;" title="'.__('Go to the last page','s2emails').'" class="last-page disabled">»</a>';
	}
	$msg .='</span></div>';
	
	if($query_pag_num > $per_page)	
		echo $msg;

	