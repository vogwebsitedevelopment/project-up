<div class="wrap">
    <?php do_action( 's2emails_admin_notices' ); ?>
    <h2><?php _e('List of Automated Emails','s2emails'); ?> <a class="add-new-h2" href="?page=s2emails-new-automated-email"><?php _e('Add New','s2emails'); ?></a></h2>
    <ul class="subsubsub">
        <li class="all"><a class="<?php echo $activeAll; ?>" href="?page=s2emails&type=all"><?php _e('All','s2emails'); ?> <span class="count">(<?php echo $all; ?>)</span></a> |</li>
        <li class="publish"><a class="<?php echo $activePublish; ?>" href="?page=s2emails&type=publish"><?php _e('Active','s2emails'); ?> <span class="count">(<?php echo $publish; ?>)</span></a> |</li>
        <li class="trash"><a class="<?php echo $activeTrash; ?>" href="?page=s2emails&type=trash"><?php _e('Trash','s2emails'); ?> <span class="count">(<?php echo $trash; ?>)</span></a></li>
    </ul>
    <form method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" id="posts-filter">
        <div class="tablenav top">
            <div class="alignleft actions bulkactions">
                <label class="screen-reader-text" for="bulk-action-selector-top"><?php _e('Select bulk action','s2emails'); ?></label>
                <select id="bulk-action-selector-top" name="bulk_action">
                    <option selected="selected" value=""><?php _e('Bulk Actions','s2emails'); ?></option><?php
                    if(isset($_REQUEST['type']) && $_REQUEST['type']=='trash')
					{	?>
                    	<option value="restore"><?php _e('Restore','s2emails'); ?></option>
                        <option value="delete"><?php _e('Delete Permanently','s2emails'); ?></option><?php
                    }
                    else
                    {	?>
                    	<option value="multi"><?php _e('Move to Trash','s2emails'); ?></option><?php
                    }	?>
                </select>
                <input type="submit" value="<?php _e('Apply','s2emails'); ?>" class="button action" id="doaction">
            </div>
            <br class="clear">
        </div>
        <table class="wp-list-table widefat fixed striped pages">
            <thead>
            <tr>
                <th class="manage-column column-cb check-column" id="cb" scope="col" style="padding:15px 0 8px 4px;">
                    <label for="cb-select-all-1" class="screen-reader-text"><?php _e('Select All','s2emails'); ?></label>
                    <input type="checkbox" id="cb-select-all-1">
                </th>
                <th class="manage-column column-title sortable desc" id="title" scope="col" style="padding-left:8px;"><?php _e('Title','s2emails'); ?></th>
               
                <th style="" class="manage-column column-title" id="subject" scope="col"><?php _e('Subject','s2emails'); ?></th>
                <th style="" class="manage-column column-title" id="schedule" scope="col"><?php _e('Sending Schedule','s2emails'); ?></th>
                <th style="" class="manage-column column-date" id="roles" scope="col"><?php _e('Send for Roles','s2emails'); ?></th>
                <th style="" class="manage-column column-date" id="sent" scope="col"><?php _e('# of Sent Emails','s2emails'); ?></th>
                <th style="" class="manage-column column-date" id="content" scope="col"><?php _e('Content Type','s2emails'); ?></th>
                <th style="" class="manage-column column-date" id="activity" scope="col"><?php _e('Activity','s2emails'); ?></th>
            </tr>
            </thead>
            <tbody id="the-list"><?php
				if($query_pag_num > 0)
				{

                    $roles = get_editable_roles();
                    $roles_names = array();
                    foreach ($roles as $key => $value){
                        $roles_names[$key] = str_replace("s2Member", "s2M. ", $value['name']);
                    }

					foreach($listData as $data)
					{
						?>
						<tr class="iedit author-self level-0 post-7 type-page status-publish hentry" id="post-7">
							<th class="check-column" scope="row">
								<label for="cb-select-<?php echo $data->id; ?>" class="screen-reader-text"><?php _e('Select Home','s2emails'); ?></label>
								<input type="checkbox" value="<?php echo $data->id; ?>" name="post[]" id="cb-select-<?php echo $data->id; ?>">
							</th>
							<td class="post-title page-title column-title">
                            	<?php $link = ($data->status==2) ? 'javascript:;' : '?page=s2emails-new-automated-email&act=edit&id='.$data->id;  ?>
                            	<a href="<?php echo $link; ?>"><?php echo $data->title; ?></a><br />
								<div class="row-actions">
                                    <?php  
									if($data->status==2)
									{	?>
										<span class="edit"><a title="<?php _e('Restore This Item','s2emails'); ?>" href="?page=s2emails&act=restore&id=<?php echo $data->id; ?>"><?php _e('Restore','s2emails'); ?></a> | </span>
										<span class="test"><a title="<?php _e('Send Test Email','s2emails'); ?>" href="?page=s2emails&act=test&id=<?php echo $data->id; ?>"><?php _e('Test Email','s2emails'); ?></a> | </span>
                                        <span class="trash"><a href="?page=s2emails&act=delete&id=<?php echo $data->id; ?>" title="Move to Trash" class="submitdelete"><?php _e('Delete Permanently','s2emails'); ?></a></span><?php
									}
									else
									{	?>	
										<span class="edit"><a title="<?php _e('Edit This Item','s2emails'); ?>" href="?page=s2emails-new-automated-email&act=edit&id=<?php echo $data->id; ?>"><?php _e('Edit','s2emails'); ?></a> | </span>
										<span class="test"><a title="<?php _e('Send Test Email','s2emails'); ?>" href="?page=s2emails&act=test&id=<?php echo $data->id; ?>"><?php _e('Test Email','s2emails'); ?></a> | </span>
                                        <span class="trash"><a href="?page=s2emails&act=trash&id=<?php echo $data->id; ?>" title="Move to Trash" class="submitdelete"><?php _e('Trash','s2emails'); ?></a></span>
										<?php
									}	?>
								</div>
							</td>	
                            	
							<td class="author column-author"><?php echo $data->subject; ?></td>
							<td class="date column-date"><?php
                                $dayt = ($data->days > 1 ? " ".__('days','s2emails') : " ".__('day','s2emails'));
                                $dayt2 = ($data->days_2 > 1 ? " ".__('days','s2emails') : " ".__('day','s2emails'));
                                $hort = ($data->hours > 1 ? " ".__('hours','s2emails') : " ".__('hour','s2emails'));
								$schedule_str = '';

                                if ($data->type_of_schedule == 1) {
                                    $schedule_str = __('after registration','s2emails');
                                } else if ($data->type_of_schedule == 2) {
                                    $schedule_str = __('before expiration','s2emails');
								} else if ($data->type_of_schedule == 3) {
                                    $schedule_str = __('after expiration','s2emails');
                                } else if ($data->type_of_schedule == 4) {
                                    $schedule_str = __('after visiting shortcode','s2emails');
                                } else if ($data->type_of_schedule == 5) {
                                    $schedule_str = __('after changing the user\'s role','s2emails');
                                } else if ($data->type_of_schedule == 6) {
                                    $schedule_str = __('after cancellation','s2emails');
                                }

                                if ($data->time_type=='1') {
                                    echo '<strong>'.$data->days.$dayt.' '.__('and','s2emails').' '.$data->hours.$hort.'</strong> '. $schedule_str;
                                } else {
                                    echo '<strong>'.$data->days_2.$dayt2.' '.__('at','s2emails').' '.date('g:i A ',strtotime($data->hours_2)).'</strong> '. $schedule_str;
                                }	
                            ?>
                          	</td>
                            <td class="author column-author">
                                <?php
                                $roles_explode = explode('|', $data->roles);
                                foreach ($roles_explode as $value) {
                                    $role_detail = explode(':', $value);
                                    if(array_key_exists(1,$role_detail)){
                                        echo $roles_names[$role_detail[0]].' (<i>'.$role_detail[1].'</i>)<br />';
                                    }else{
                                        echo (isset($roles_names[$role_detail[0]]) ? $roles_names[$role_detail[0]] : '').'<br />';
                                    }
                                }
                                ?></td> 
                            <td class="author column-author"><?php echo $data->counter; ?></td>     	
                            <td class="date column-date"><?php
								if($data->content_type=='1') {
									 _e('Plain/Text','s2emails');
								} else {
									_e('Text/Html','s2emails');
								}
                            ?>    
                          	</td>
                            <td class="date column-date"><?php
                                if($data->active=='1') {
                                     echo "<strong>".__('Active','s2emails')."</strong>";
                                } else {
                                    echo "<strong>".__('Deactivated','s2emails')."</strong>";
                                }
                            ?>
                            </td>
						</tr><?php
					}
				}
				else
				{
					echo '<tr class="no-items"><td colspan="8" class="colspanchange">'. __('No automated emails have been created yet','s2emails') .'</td></tr>';
				}?>
            </tbody>
            <tfoot>
                <tr>
                    <th class="manage-column column-cb check-column" scope="col" style="padding:15px 0 8px 4px;">
                        <label for="cb-select-all-2" class="screen-reader-text"><?php _e('Select All','s2emails'); ?></label>
                        <input type="checkbox" id="cb-select-all-2">
                    </th>
                    <th style="padding-left:8px;" class="manage-column column-title sortable desc" id="title" scope="col"><?php _e('Title','s2emails'); ?></th>
                    <th style="" class="manage-column column-title" id="subject" scope="col"><?php _e('Subject','s2emails'); ?></th>
                    <th style="" class="manage-column column-title" id="schedule" scope="col"><?php _e('Sending Schedule','s2emails'); ?></th>
                    <th style="" class="manage-column column-date" id="roles" scope="col"><?php _e('Send for Roles','s2emails'); ?></th>
                    <th style="" class="manage-column column-date" id="sent" scope="col"><?php _e('Sent Emails','s2emails'); ?></th>
                    <th style="" class="manage-column column-date" id="content" scope="col"><?php _e('Content Type','s2emails'); ?></th>
                    <th style="" class="manage-column column-date" id="activity" scope="col"><?php _e('Activity','s2emails'); ?></th>
                </tr>
            </tfoot>
        </table>
        <div class="tablenav bottom">
            <div class="alignleft actions bulkactions">
                <label class="screen-reader-text" for="bulk-action-selector-bottom"><?php _e('Select bulk action','s2emails'); ?></label>
                <select id="bulk-action-selector-bottom" name="bulk_action2">
                    <option selected="selected" value=""><?php _e('Bulk Actions','s2emails'); ?></option><?php
                    if(isset($_REQUEST['type']) && $_REQUEST['type']=='trash')
					{	?>
                    	<option value="restore"><?php _e('Restore','s2emails'); ?></option>
                        <option value="delete"><?php _e('Delete Permanently','s2emails'); ?></option><?php
                    }
                    else
                    {	?>
                    	<option value="multi"><?php _e('Move to Trash','s2emails'); ?></option><?php
                    }	?>
                </select>
                <input type="submit" value="<?php _e('Apply','s2emails'); ?>" class="button action" id="doaction2">
            </div>
            <div class="alignleft actions"></div>
           	<?php include(S2EMAILS_TPL_DIR .'_partials/pagination.php'); ?>
        </div>
    </form>
    <div id="ajax-response"></div>
    <br class="clear">
</div>