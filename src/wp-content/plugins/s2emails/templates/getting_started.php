<div class="wrap">
    <h2><?php _e('Getting started','s2emails'); ?></h2> 
    <div>
    	<h3><?php _e('What should I do first?','s2emails'); ?></h3>
    	<p>
    	<ol>
	    	<li><?php _e('Open General Options and set the address you want to send your emails from and your local timezone.','s2emails'); ?></li>
            <li><?php _e('To send also cancellation emails open <strong>s2Member (Pro) > API Notifications</strong>','s2payments'); ?><br />
                <?php _e('Open "<strong>Cancellation Notifications</strong>" tab','s2payments'); ?><br />
                <?php _e('Insert there this code: (the whole link is 1 line!)','s2payments'); ?><textarea id="s2membercode"><?php echo site_url(); ?>/?s2_cancellation_notification=yes&user_id=%%user_id%%</textarea>
                <?php _e('Feel free to skip this step if you don\'t plan to send emails after subscription cancellation.','s2payments'); ?></li>
	    	<li><?php _e('Set up cron in case you don\'t have at least one visitor per hour.','s2emails'); ?></li>
	    	<li><?php _e('Create your first automated email.','s2emails'); ?></li>
	    	<li><?php _e('Sit back and enjoy!','s2emails'); ?></li>
    	</ol>
    	</p>

    	<h3><?php _e('How to set up the cron?','s2emails'); ?></h3>
    	<p>
    		1. <?php _e('If you have at least a few visitors a day, all emails will be sent automatically.','s2emails'); ?><br />
            2. <?php _e('The other option is to set up a cron job that calls <strong>yourwebsite.com/wp-cron.php</strong> every 10 minutes (*/10 * * * *)','s2emails'); ?>
    	</p>

    	<h3><?php _e('How many emails can I send?','s2emails'); ?></h3>
    	<p>
    		<?php _e('S2Emails doesn\'t set any limit on sent emails. There are probably some limitations from your hosting provider. However, if you are not getting hundred of new members every day, you don\'t have to be worried.','s2emails'); ?>
    	</p>

    	<h3><?php _e('Does the plugin also work without s2Member?','s2emails'); ?></h3>
    	<p>
    	   <?php _e('No, s2Emails requires s2Member. Otherwise, it will not function; no emails will be sent.','s2emails'); ?>
    	</p>

    	<h3><?php _e('Who will be the recipient of the test emails?','s2emails'); ?></h3>
    	<p>
	    	<?php _e('They will be sent to the email you have specified in "General Options" as the sender. (You can send a test email from "Automated Emails" page)','s2emails'); ?>
    	</p>

        <h3><?php _e('Can I turn off sending automated emails to a particular user?','s2emails'); ?></h3>
        <p>
            <?php _e('Yes. Open the profile page of the user and set <strong>Enable Reminder Emails</strong> to <strong>No (exclude)</strong>.','s2emails'); ?>
        </p>

        <h3><?php _e('What is the shortcode {user_role} good for?','s2emails'); ?></h3>
        <p>
            <?php _e('This shortcode will be replaced by the current role of the user.','s2emails'); ?>
        </p>

        <h3><?php _e('What is the shortcode {new_password} good for?','s2emails'); ?></h3>
        <p>
            <?php _e('This shortcode will be replaced by a link that will allow the user to reset his/her password.','s2emails'); ?>
        </p>

        <h3><?php _e('What is the shortcode {unsubscribe}...{/unsubscribe} good for?','s2emails'); ?></h3>
        <p>
            <?php _e('This shortcode will be replaced by a link that will allow the user to unsubscribe from the s2emails. You can use it like this: {unsubscribe}Unsubscribe me!{/unsubscribe}','s2emails'); ?>
        </p>
    </div>
</div>