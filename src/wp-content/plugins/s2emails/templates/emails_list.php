<?php 
global $wpdb;
$table = $wpdb->prefix.'s2emails';
$s2emails_repo = new S2emailRepository();

if(isset($_REQUEST['act']) && $_REQUEST['act'] == 'trash') {
	if ($wpdb->query("UPDATE `".$table."` SET status = 2 WHERE id  = '".$_REQUEST['id']."' ")) {
		$s2emails_repo->updatePendingMailsByMailId($_REQUEST['id'], 2);
	}
} elseif(isset($_REQUEST['act']) && $_REQUEST['act']=='restore') {
	if ($wpdb->query("UPDATE `".$table."` SET status = 1 WHERE id  = '".$_REQUEST['id']."' ")) {
		$s2emails_repo->updatePendingMailsByMailId($_REQUEST['id'], 1);
	}
} elseif(isset($_REQUEST['act']) && $_REQUEST['act']=='delete') {
	$getRow = $wpdb->get_row("SELECT * FROM `".$table."` WHERE id  = '".$_REQUEST['id']."' ");
	update_post_meta($getRow->post_id, 'wpcf-status', 1);
	
	if ($wpdb->delete($table, array('id' => $_REQUEST['id']), array('%d'))) {
		$s2emails_repo->deletePendingMailsByTemplateId($_REQUEST['id']);
	}
} elseif((isset($_REQUEST['bulk_action']) && $_REQUEST['bulk_action']=='multi') || (isset($_REQUEST['bulk_action2']) && $_REQUEST['bulk_action2']=='multi') ) {
	if ($wpdb->query("UPDATE `".$table."` SET status = 2 WHERE id IN(".join(',',$_POST['post']).")")) {
		$s2emails_repo->updatePendingMailsByMailId($_REQUEST['post'], 2);
	}
} elseif((isset($_REQUEST['bulk_action']) && $_REQUEST['bulk_action']=='restore') || (isset($_REQUEST['bulk_action2']) && $_REQUEST['bulk_action2']=='restore') ) {
	if ($wpdb->query("UPDATE `".$table."` SET status = 1 WHERE id IN (".join(',',$_POST['post']).")")) {
		$s2emails_repo->updatePendingMailsByMailId($_POST['post'], 1);
	}
} elseif((isset($_REQUEST['bulk_action']) && $_REQUEST['bulk_action']=='delete') || (isset($_REQUEST['bulk_action2']) && $_REQUEST['bulk_action2']=='delete') ) {
	if ($wpdb->query("DELETE FROM `".$table."` WHERE id IN (".join(',',$_POST['post']).")")) {
		foreach ($_POST['post'] as $id) {
			if ((int)$id) {
				$s2emails_repo->deletePendingMailsByTemplateId($id);
			}
		}
	}
} elseif(isset($_REQUEST['act']) && $_REQUEST['act']=='test') {
	$automated_email = $wpdb->get_row("SELECT * FROM ".$table." WHERE id = '".$_REQUEST['id']."' ");

	if($automated_email->content_type==1) {
		$headers = array('Content-Type: text/html; charset=UTF-8');
		$message_body = html_entity_decode(nl2br($automated_email->content));
	} else {
		$headers = array('Content-Type: text/html; charset=UTF-8');
		$message_body = html_entity_decode($automated_email->content);
	}	

	$subject = $automated_email->subject;
	$mail_body = $message_body;
	$from_email = get_option('s2emails_from_email');
	$from_title =  get_option('s2emails_from_title');

	$copy_email = (get_option('s2emails_copy_email'))? get_option('s2emails_copy_email') : false;

	$headers[] = 'From: '.$from_title.' <'.$from_email.'>';

	if($copy_email != false):
		$to = $copy_email;
	else:
		$to = $from_email;
	endif;

	wp_mail( $to, $subject, $mail_body, $headers );
}

function s2emails_admin_updated_message() {
	$updated_message = '';

    if ( empty( $_REQUEST['act'] ) && empty( $_REQUEST['bulk_action'] )) {
        return;
    }

    if ( 'test' == $_REQUEST['act'] ) {
        $updated_message = __( "Test email has been sent.", 's2emails' );
    } else if( 'new' == $_REQUEST['act'] ) {
    	$updated_message = __( "New email created.", 's2emails' );
    } else if( 'edit' == $_REQUEST['act'] ) {
    	$updated_message = __( "Email updated.", 's2emails' );	
	} else if( 'delete' == $_REQUEST['act'] ) {
    	$updated_message = __( "Email deleted permanently.", 's2emails' );
	} else if( 'trash' == $_REQUEST['act'] ) {
    	$updated_message = __( "Email moved to trash.", 's2emails' );
   	} else if( 'restore' == $_REQUEST['act'] ) {
    	$updated_message = __( "Email restored.", 's2emails' );
   	} else if( 'multi' == $_REQUEST['bulk_action'] ) {
    	$updated_message = __( "Emails moved to trash.", 's2emails' );
   	} else if( 'restore' == $_REQUEST['bulk_action'] ) {
    	$updated_message = __( "Emails restored.", 's2emails' );
   	} else if( 'delete' == $_REQUEST['bulk_action'] ) {
    	$updated_message = __( "Emails deleted permanently.", 's2emails' );
   	}

    if ( ! empty( $updated_message ) ) {
        echo sprintf( '<div id="message" class="updated notice notice-success is-dismissible"><p>%s</p></div>', esc_html( $updated_message ) );
    }
}

add_action( 's2emails_admin_notices', 's2emails_admin_updated_message' );
// display notifications where an action took place
add_action( 'admin_notices', 's2emails_admin_updated_message' );

$page = (isset($_REQUEST['p']) && !empty($_REQUEST['p'])) ? $_REQUEST['p'] : 1;
$cur_page = $page;
$page -= 1;
$per_page = 20;
$previous_btn = true;
$next_btn = true;
$first_btn = true;
$last_btn = true;
$start = $page * $per_page;
$activePublish = '';

if(isset($_REQUEST['type']) && $_REQUEST['type']=='trash')
{
	$condition = "WHERE status = 2";
}
elseif(isset($_REQUEST['type']) && $_REQUEST['type']=='all')
{
	$condition = "WHERE status = 2 or status = 1";
}
else
{
	$condition = "WHERE status = 1";
}

$activeAll = (isset($_REQUEST['type']) && $_REQUEST['type']=='all')? 'current' : '';
$activeTrash = (isset($_REQUEST['type']) && $_REQUEST['type']=='trash')? 'current' : '';
$activePublish = ((isset($_REQUEST['type']) && $_REQUEST['type']=='publish') || !isset($_REQUEST['type']))? 'current' : '';
	
$listData =$wpdb->get_results("SELECT * from `".$table."` ".$condition." ORDER BY type_of_schedule ASC, days ASC , days_2 ASC , hours ASC , hours_2, roles ASC LIMIT ".$start.", ".$per_page."");
$wpdb->get_results("SELECT * FROM `".$table."` ".$condition."");
$query_pag_num = $wpdb->num_rows;

$wpdb->get_results("SELECT * FROM `".$table."` WHERE status = 1 ");
$publish = $wpdb->num_rows;

$wpdb->get_results("SELECT * FROM `".$table."` WHERE status = 2 ");
$trash = $wpdb->num_rows;

$all = $trash + $publish;

if(isset($_REQUEST['type']) && $_REQUEST['type']=='trash')
{
	$url = admin_url().'admin.php?page=s2emails&type=trash';
}
elseif(isset($_REQUEST['type']) && $_REQUEST['type']=='publish')
{
	$url = admin_url().'admin.php?page=s2emails&type=publish';
}
elseif(isset($_REQUEST['type']) && $_REQUEST['type']=='all')
{
	$url = admin_url().'admin.php?page=s2emails&type=all';
}
else
{
	$url = admin_url().'admin.php?page=s2emails';
} 

S2emailTemplateManager::s2emailsListOfAutomatedEmails(array(
    'publish'       => $publish,
    'trash'         => $trash,
    'all'			=> $all,
    'url'           => $url,
    'listData'      => $listData,
    'activeAll'     => $activeAll,
    'activePublish' => $activePublish,
    'activeTrash'   => $activeTrash,
    'query_pag_num' => $query_pag_num,
    'per_page'      => $per_page,
    'cur_page'      => $cur_page,
    'first_btn'     => $first_btn,
    'previous_btn'  => $previous_btn,
    'next_btn'      => $next_btn,
    'last_btn'      => $last_btn
));
?>