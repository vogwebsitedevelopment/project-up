<?php
global $wpdb;

$page = (isset($_REQUEST['p']) && !empty($_REQUEST['p'])) ? $_REQUEST['p'] : 1;
$cur_page = $page;
$page -= 1;
$per_page = 150;
$previous_btn = true;
$next_btn = true;
$first_btn = true;
$last_btn = true;
$start = $page * $per_page;
$activePublish = '';
$menu_slug = 's2emails-list-pending-emails';
$get_status = null;

if(isset($_REQUEST['type'])) {
    if ($_REQUEST['type'] == 'trash') {
        $get_status = 2;
    } else if ($_REQUEST['type'] == 'publish') {
        $get_status = 0;
    } else if ($_REQUEST['type'] == 'sent') {
        $get_status = 1;
    }
} else {
    $get_status = 0;
}

if(isset($_GET['action'])) {
    if ($_GET['action'] == 'activate') {
        S2emailRepository::updateStaticPendingMails(array('status' => 0), array('id' => (int)$_GET['id_pending']));
    } elseif ($_GET['action'] == 'deactivate') {
        S2emailRepository::updateStaticPendingMails(array('status' => 2), array('id' => (int)$_GET['id_pending']));
    }
}

if (isset($_REQUEST['bulk_action'])) {
    if (isset($_REQUEST['post']) && count($_REQUEST['post'])) {
        if ($_REQUEST['bulk_action'] == 'deactivate_mails') {       
            foreach ($_REQUEST['post'] as $id_mail) {
                S2emailRepository::updateStaticPendingMails(array('status' => 2), array('id' => (int)$id_mail));
            }
        }

        if ($_REQUEST['bulk_action'] == 'activate_mails') {       
            foreach ($_REQUEST['post'] as $id_mail) {
                S2emailRepository::updateStaticPendingMails(array('status' => 0), array('id' => (int)$id_mail));
            }
        }
    }
}

$activeAll      = (isset($_REQUEST['type']) && $_REQUEST['type']=='all')? 'current' : '';
$activeTrash    = (isset($_REQUEST['type']) && $_REQUEST['type']=='trash')? 'current' : '';
$activeSent     = (isset($_REQUEST['type']) && $_REQUEST['type']=='sent')? 'current' : '';
$activePublish  = ((isset($_REQUEST['type']) && $_REQUEST['type']=='publish') || !isset($_REQUEST['type']))? 'current' : '';

$listData = S2emailRepository::getPendingMails($get_status, $start, $per_page);
$pending_mails = $listData['obj'];
$query_pag_num = $listData['num_rows'];
// pagination
$get_rows_pagi = S2emailRepository::getPendingMails($get_status);
$count_pages   = $get_rows_pagi['num_rows'];
// all pendings
$allData     = S2emailRepository::getPendingMails();
$allDataCount= $allData['num_rows'];
// published pendings
$publisData = S2emailRepository::getPendingMails(0);
$publish    = $publisData['num_rows'];
// trashed pendings
$trashData  = S2emailRepository::getPendingMails(2);
$trash      = $trashData['num_rows'];
// sent mails
$sentData  = S2emailRepository::getPendingMails(1);
$sentMails = $sentData['num_rows'];

$url = admin_url().'admin.php?page='.$menu_slug;
$pending_url = $url;
if (isset($_REQUEST['type']) && $_REQUEST['type']=='trash') {
    $url .= '&type=trash';
} elseif (isset($_REQUEST['type']) && $_REQUEST['type']=='publish') {
    $url .= '&type=publish';
} elseif (isset($_REQUEST['type']) && $_REQUEST['type']=='all') {
    $url .= '&type=all';
} elseif (isset($_REQUEST['type']) && $_REQUEST['type']=='sent') {
    $url .= '&type=sent';
}
?>

<div class="wrap">
    <h2><?php _e('List of Pending Emails'); ?></h2>
    <ul class="subsubsub">
        <li class="all"><a class="<?php echo $activeAll; ?>" href="<?php echo $pending_url; ?>&type=all"><?php _e('All','s2emails'); ?> <span class="count">(<?php echo $allDataCount; ?>)</span></a> |</li>
        <li class="publish"><a class="<?php echo $activePublish; ?>" href="<?php echo $pending_url; ?>&type=publish"><?php _e('Active','s2emails'); ?> <span class="count">(<?php echo $publish; ?>)</span></a> |</li>
        <li class="trash"><a class="<?php echo $activeTrash; ?>" href="<?php echo $pending_url; ?>&type=trash"><?php _e('Trash','s2emails'); ?> <span class="count">(<?php echo $trash; ?>)</span></a> |</li>
        <li class="sent"><a class="<?php echo $activeSent; ?>" href="<?php echo $pending_url; ?>&type=sent"><?php _e('Sent','s2emails'); ?> <span class="count">(<?php echo $sentMails; ?>)</span></a></li>
    </ul>
    <form method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" id="posts-filter">
        <div class="tablenav top">
            <div class="alignleft actions bulkactions">
                <label class="screen-reader-text" for="bulk-action-selector-top"><?php _e('Select bulk action','s2emails'); ?></label>
                <select id="bulk-action-selector-top" name="bulk_action">
                    <option selected="selected" value=""><?php _e('Bulk Actions','s2emails'); ?></option>
                <?php if (isset($_REQUEST['type']) && in_array($_REQUEST['type'], array('all', 'trash'))): ?>
                    <option value="activate_mails"><?php _e('Activate','s2emails'); ?></option>
                <?php endif; ?>
                <?php if ((isset($_REQUEST['type']) && in_array($_REQUEST['type'], array('all', 'publish'))) || !isset($_REQUEST['type'])): ?>
                    <option value="deactivate_mails"><?php _e('Deactivate','s2emails'); ?></option>
                <?php endif; ?>
                </select>
                <input type="submit" value="<?php _e('Apply','s2emails'); ?>" class="button action" id="doaction">
            </div>
            <br class="clear">
        </div>
        <table class="wp-list-table widefat fixed striped pages">
            <thead>
            <tr>
                <th class="manage-column column-cb check-column" id="cb" scope="col" style="padding:15px 0 8px 4px;">
                    <label for="cb-select-all-1" class="screen-reader-text"><?php _e('Select All','s2emails'); ?></label>
                    <input type="checkbox" id="cb-select-all-1">
                </th>
                <th class="manage-column column-title sortable desc" id="title" scope="col" style="padding-left:8px;"><?php _e('Email Address','s2emails'); ?></th>
                <th style="" class="manage-column column-title" id="title" scope="col"><?php _e('Name of the template','s2emails'); ?></th>
                <th style="" class="manage-column column-tags" id="title" scope="col">
                <?php
                    if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'sent') {
                        _e('Sent on','s2emails');
                    } else {
                        _e('When to send','s2emails');
                    }
                 ?></th>
                <th style="" class="manage-column column-tags" id="title" scope="col"><?php _e('Action','s2emails'); ?></th>
            </tr>
            </thead>
            <tbody id="the-list">
                <?php
				if ($query_pag_num > 0) {
					foreach($pending_mails as $data) {
					?>
						<tr class="iedit author-self level-0 post-7 type-page status-publish hentry" id="post-7">
							<th class="check-column" scope="row">
                                <label for="cb-select-<?php echo $data->id; ?>" class="screen-reader-text"><?php _e('Select Home','s2emails'); ?></label>
                                <input type="checkbox" value="<?php echo $data->id; ?>" name="post[]" id="cb-select-<?php echo $data->id; ?>">
                            </th>
							<td class="post-title page-title column-title">
                            	<?php
                                    $user_info = get_userdata($data->id_user);
                                    echo $user_info->user_email;
                                    if($data->reminder == 0){
                                        echo ' <i>'.__('(unsubscribed)','s2emails').'</i>';
                                    }
                                ?><br />
                                <div class="row-actions">
                                    <span class="edit"><?php 
                                    if ($data->status == 0 || $data->status == 2) {
                                        if ($data->status == 0) {
                                            $link_query = '&action=deactivate';
                                            $title = __('Deactivate','s2emails');
                                        } else if ($data->status == 2) {
                                            $link_query = '&action=activate';
                                            $title = __('Activate','s2emails');
                                        }
                                        
                                        echo '<a href="'. $pending_url . $link_query .'&id_pending='. $data->id .'">'. $title .'</a>';
                                    }
                                    ?>
                                    </span> 
                                </div>
							</td>			
							<td class="post-title page-title column-title">
                                <?php 
                                    $template = S2emailRepository::getStaticTemplateMailById($data->id_mail);
                                    if ($template) {
                                        echo $template->title;
                                    }
                                ?>
                          	</td>
                            <td class="date column-tags">
                            <?php
                                $whenToSend = new DateTime($data->send_mail_at);
                                $today      = new DateTime('now');
                                // add cron job duration to check
                                $getS2emailCronJob = (get_option('s2emails_cron_job_duration') ?: 10);
                                $today->add(new DateInterval('PT'. $getS2emailCronJob .'M'));

                                if ($today > $whenToSend && $data->status == 0) {
                                    echo _e('About to send','s2emails');
                                } else {
                                    echo $whenToSend->format('Y-m-d H:i:s');
                                }
                            ?>
                          	</td>
                            <td class="date column-tags ">
                               <a href="?page=s2emails-new-automated-email&act=edit&id=<?php echo $data->id_mail; ?>" class="view_cont" title="<?php _e('Edit template','s2emails'); ?>"><?php _e('Edit template','s2emails'); ?></a></a>
                          	</td>
						</tr><?php
					}
                $query_pag_num = $count_pages;
				} else {
					echo '<tr class="no-items"><td colspan="4" class="colspanchange">'. __('No pending emails at the moment','s2emails') .'</td></tr>';
				}
            ?>
            </tbody>
            <tfoot>
                <tr>
                    <th class="manage-column column-cb check-column" scope="col" style="padding:15px 0 8px 4px;">
                        <label for="cb-select-all-2" class="screen-reader-text"><?php _e('Select All','s2emails'); ?></label>
                        <input type="checkbox" id="cb-select-all-2">
                    </th>
                    <th class="manage-column column-title sortable desc" id="title" scope="col" style="padding-left:8px;"><?php _e('Email Address','s2emails'); ?></th>
                    <th style="" class="manage-column column-title" id="title" scope="col"><?php _e('Name of the template','s2emails'); ?></th>
                    <th style="" class="manage-column column-tags" id="title" scope="col"><?php _e('When to send','s2emails'); ?></th>
                    <th style="" class="manage-column column-tags" id="title" scope="col"><?php _e('Action','s2emails'); ?></th>
                </tr>
            </tfoot>
        </table>
        <div class="tablenav bottom">
            <div class="alignleft actions"></div>
           	<?php include(S2EMAILS_TPL_DIR .'_partials/pagination.php'); ?>
        </div>
    </form>
    <div id="ajax-response"></div>
    <br class="clear">
</div>
