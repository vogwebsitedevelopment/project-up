<?php
/*
Plugin Name: s2Emails (Pro)
Plugin URI: 
Description: s2Emails is an addon for s2Member - send automated emails
Author: s2Plugins.com
Version: 2.1.4
Author URI: https://s2plugins.com

Copyright 2016 - 2018 s2Plugins s2plugins@gmail.com

*/	

if ( ! defined( 'WPINC' ) ) {
	die;
}

define('PLUGIN_PATH', WP_PLUGIN_DIR . '/s2emails/');
define('MAIN_PLUGIN_FILE', PLUGIN_PATH . 's2emails.php');
define('S2EMAILS_TPL_DIR', PLUGIN_PATH . 'templates/');
define('REL_PLUGIN_PATH', plugin_dir_url( __FILE__ ));

require_once( PLUGIN_PATH . 'classes/S2pluginChecker.php' );
require_once( PLUGIN_PATH . 'classes/S2emailCore.php' );
require_once( PLUGIN_PATH . 'classes/S2emailInitializer.php' );
require_once( PLUGIN_PATH . 'classes/S2emailTemplateManager.php' );
require_once( PLUGIN_PATH . 'classes/S2emailRepository.php' );

$s2emailsCore = new S2emailCore((new S2emailInitializer()), (new S2emailRepository()));
$s2emailsCore->run();

require_once( dirname( __FILE__ ) . '/plugin-updates/plugin-update-checker.php' );
$MyUpdateChecker = PucFactory::buildUpdateChecker(
    'https://s2plugins.com/wp-content/updater/s2emails.json',
    __FILE__,
    's2emails'
);

?>
