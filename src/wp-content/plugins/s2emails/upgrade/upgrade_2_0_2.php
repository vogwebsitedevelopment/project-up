<?php

function upgrade_2_0_2()
{
	global $wpdb;

	$s2emails = $wpdb->get_row("SHOW COLUMNS FROM {$wpdb->prefix}s2emails LIKE 'new_roles'");

	//Add column if not present.
	if(!$s2emails){
	    $wpdb->query("ALTER TABLE ".$wpdb->prefix."s2emails ADD new_roles TEXT");
	}

    return true;
}