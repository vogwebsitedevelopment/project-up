<?php

function upgrade_2_1_0()
{
	global $wpdb;

	$s2emails = $wpdb->get_row("SHOW COLUMNS FROM {$wpdb->prefix}s2emails LIKE 'cc_emails'");

	//Add cc_emails column if not present.
	if(!$s2emails){
	    $wpdb->query("ALTER TABLE ".$wpdb->prefix."s2emails ADD cc_emails TEXT NULL DEFAULT NULL"); 
	}

	// update pending table, add new enum 'after_cancel'
	$wpdb->query("ALTER TABLE `". $wpdb->prefix ."s2emails_pending` CHANGE `mail_type` `mail_type` ENUM('after_reg','before_exp','after_exp','shortcode','role_update','after_cancel') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL");

	// check if mu-plugins dir doesn't exist, create it
	$mu_plugins_dir = dirname(__FILE__) .'/../../../mu-plugins';
	if ( !file_exists( $mu_plugins_dir ) ) {
		@mkdir($mu_plugins_dir);
	}

	// copy new cancellation notifation mu-plugin file to mu-plugins folder
	$targeted_file = $mu_plugins_dir .'/s2-emails-cancellation.php';
	if ( !file_exists( $targeted_file ) ) {
		$origin_file = dirname(__FILE__) .'/../mu-plugins/s2-emails-cancellation.php';
		@copy( $origin_file, $targeted_file );
		@unlink( $origin_file );
	}

	// create logs folder
	$log_dir = dirname(__FILE__) .'/../logs';
	if ( !file_exists( $log_dir ) ) {
		@mkdir( $log_dir, 0755 );
	}

    return true;
}