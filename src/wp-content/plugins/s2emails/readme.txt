=== s2Emails ===

Version: 2.1.4

Tested up to: 4.8
Requires at least: 4.2

Requires PHP: 5.2+
Tested up to PHP: 7.0

Copyright: © 2018 Willion s.r.o.
License: GNU General Public License v2 or later.
Contributors: Yassine Belkaid, Andrej Staš

Author: s2Plugins.com / Willion s.r.o.
Author URI: https://s2plugins.com/

Text Domain: s2emails
Domain Path: /languages

Plugin Name: s2Emails
Plugin URI: https://s2plugins.com/
Changelog URI: https://s2plugins.com/versions/

Description: s2Emails is a smart plugin for WordPress® which helps you to maintain the relationship with your members by sending automated emails.
Tags: s2email, s2emails, s2, s2member, s2 member, membership, users, user, members, member, subscribers, subscriber, email, emails, emailing, renewal reminder, renewal reminders, reminders, registration, expiration, smart emailing, automated emails, automatic emails,

== Installation ==

= s2Emails is Very Easy to Install =

1. Upload the `/s2emails` folder to your `/wp-content/plugins/` directory.
2. Activate the plugin through the **Plugins** menu in WordPress®.
3. Navigate to the **s2Emails > General Options** panel for configuration details.

== License ==

Copyright: © 2018 [Willion, s.r.o.]

Released under the terms of the [GNU General Public License](http://www.gnu.org/licenses/gpl-2.0.html).
