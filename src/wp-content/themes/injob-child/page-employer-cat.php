<?php get_header(); ?>
<?php $styleDir = get_stylesheet_directory_uri(); ?>


<div class="contents-main" id="contents-main" style="padding-top: 150px;">
	
    
        <div class="entry-content">
            <div class="vc_row wpb_row vc_row-fluid category-div  vc_row-has-fill">
                <div class="container" style="background-color: #fff;">
                    <div class="row">
                        <div class="wpb_column vc_column_container vc_col-sm-12" style="padding-left: 0;padding-right: 0;width: 100%;">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                                    <div class="wpb_raw_code wpb_content_element" style="margin-bottom:35px;" >
                                                        <div class="wpb_wrapper">
                                                             <div class="row align-center f-row categories-list">
                                                                <div class="columns small-12 medium-12 sp-banner-container ">
                                                                    <h2>Available Categories</h2>
                                                                </div>
                                                           </div>
                                                        </div>
                                                    </div>
                                                <div class="iw-heading  style1  text-center border-color-theme" style="width: 100%">

                                                </div>
                                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                    <div class="job-cats-list wpb_column vc_column_container vc_col-sm-12">
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">   
                                                                 <div class="iwj-categories style1 ">
                                                                           <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-bar-chart"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Accounting</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-mobile"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">App Design</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-building-o"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Architect</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="ion-android-color-palette"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Arts, Design, Media</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                <div class="clearfix"></div>                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-bank"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Banking</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-calculator"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Business Accountant</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="ion-ios-photos"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Carpet</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-road"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Civil Engineer</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                <div class="clearfix"></div>                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-th-large"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Concrete</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="ion-hammer"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Contractor</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-pause"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Doors</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-tint"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Earthwork</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                <div class="clearfix"></div>                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="ion-university"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Education &amp; Coaching</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-bolt"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Electrical</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-code-fork"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Electrical Engineer</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="ion-ios-speedometer"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Engineering</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                <div class="clearfix"></div>                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-columns"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Exterior Finishes</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-list"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Fence work</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="ion-ios-pie"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Financial Services</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-sign-language"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">General Cleaning</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                <div class="clearfix"></div>                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-gavel"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">General Contractor</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-file-image-o"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Graphic Design</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-database"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">HVAC</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-scissors"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Interior Decorating</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                <div class="clearfix"></div>                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-bed"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Interior Design</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-bath"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Interior Finishes</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-table"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Masonry</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-car"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Mechanical Engineer</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                <div class="clearfix"></div>                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-cogs"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Metal work</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-truck"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Motoring &amp; Automotive</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-paint-brush"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Paint</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-shower"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Plumbing</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                <div class="clearfix"></div>                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="ion-android-bar"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Restaurant</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-home"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Roof work</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="ion-speakerphone"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Sales &amp; Marketing</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-snowflake-o"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Snow Removal</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                <div class="clearfix"></div>                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-align-center"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Structural Engineer</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="ion-ios-paper"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Taxes &amp; Bookkeeping</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-thermometer-full"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Thermal &amp; Moisture Insulation</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-th"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Tile work</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                <div class="clearfix"></div>                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-trash"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Waste Removal</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-laptop"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Web Design</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="ion-code"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Web Developer</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-window-maximize"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Window Cleaner</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                <div class="clearfix"></div>                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-minus-square-o"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Windows</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-tree"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Woodwork</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="ion-ios-sunny"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Yard &amp; Landscape work</a></h3>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                                   <div class="item-category" style="padding-top: 47px;"> 
                                                                        <div class="item-category-inner">
                                                                            <span class="category-icon"><i class="fa fa-leaf"></i></span>
                                                                            <h3 class="category-title"><a href="https://project-up.ca/dashboard/?iwj_tab=new-job">Yard Care</a></h3>
                                                                        
                                                                        </div>
                                                                    </div>
                                                                    </div>
    </div>
</div>
</div>
</div>
        <div class="wpb_raw_code wpb_content_element wpb_raw_html" >
            <div class="wpb_wrapper" style="padding-bottom: 150px;">
                <p class="all-cat-message" style="text-align: center;padding-top: 50px;font-size: 18px;">Not seeing what you are looking for ? <a href="/pre-register">Request a Category</a></p>
    
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>

<?php get_footer(); ?>