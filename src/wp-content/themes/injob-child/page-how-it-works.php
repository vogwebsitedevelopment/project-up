<?php get_header(); ?>
<?php $styleDir = get_stylesheet_directory_uri(); ?>

<div class="fullwidth" id="topper">
	<div class="row f-row align-center align-middle">
		<div class="columns small-12 medium-6 show-for-medium">
			<img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full') ?>" alt="ProjectUp logo">
		</div>
		<div class="columns small-12 medium-6">
			<h1><?php the_title();?></h1>
			<p><?php the_field('descriptor_line');?></p>
		</div>
	</div>
</div>
<div class="fullwidth" id="tabs">
	<div class="row align-center f-row">
		
		<div class="columns small-12 medium-11 tab-box">
			<div class="tabs-content">
				<div class="row f-row tabs-panel is-active" id="personal-panel">
					<?php
					$i=1;
						// check if the repeater field has rows of data
						if( have_rows('personal_tab') ){
							// loop through the rows of data
							while ( have_rows('personal_tab') ) {
								the_row();
								$n=$i++;?>
								<div class="columns small-12 medium-4">
									<div class="number">
										<span><?php echo $n;?></span>
									</div>
									<div class="image">
										<img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('description');?>"> 
									</div>
									<div class="description">
										<p><?php the_sub_field('description');?></p>
									</div>
								</div>
								<?php
						}
						}else{
							// no rows found
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="fullwidth" id="tabs">
	<div class="row align-center f-row">

	<div class="columns small-12 medium-11 sp-banner-container">
			<h2>Post Your Project</h2>
		</div>

		<div class="columns small-12 medium-11 text-content">
			<div class="padding2rem">
				<?php the_field('post_your_project')?>
			</div>
		</div>
		<div class="columns small-12 medium-11 sp-banner-container">
			<h2>Service Providers</h2>
		</div>
		<div class="columns small-12 medium-11 text-content">
			<div class="padding2rem">
				<?php the_field('service_providers')?>
			</div>
			<div class="padding2rem">
			
			<a href="/service-provider-registration" class="button text-center">Service Provider Registration</a>
					</div>

		</div>
		<div class="columns small-12 medium-11 tab-box tab-box2">
			<div class="tabs-content">
				<div class="row f-row tabs-panel is-active" id="personal-panel">
					<?php
					$i=1;
						// check if the repeater field has rows of data
						if( have_rows('business_tab') ){
							// loop through the rows of data
							while ( have_rows('business_tab') ) {
								the_row();
								$n=$i++;?>
								<div class="columns small-12 medium-4">
									<div class="number">
										<span><?php echo $n;?></span>
									</div>
									<div class="image">
										<img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('description');?>"> 
									</div>
									<div class="description">
										<p><?php the_sub_field('description');?></p>
									</div>
								</div>
								<?php
						}
						}else{
							// no rows found
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>