<?php get_header(); ?>
<?php $styleDir = get_stylesheet_directory_uri(); ?>

<div class="fullwidth" id="topper">
	<div class="row f-row align-center align-middle">
		<div class="columns small-12 medium-6 show-for-medium">
			<img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full') ?>" alt="ProjectUp logo">
		</div>
		<div class="columns small-12 medium-6">
			<h1><?php the_title();?></h1>
			<p><?php the_field('descriptor_line');?></p>
		</div>
	</div>
</div>
<div class="fullwidth">
	<div class="row align-center f-row">
		<div class="columns small-12 medium-11 sp-banner-container">
			<h2>Why are we doing this</h2>
		</div>
		<div class="columns small-12 medium-11 text-content padding2rem">
			<?php the_content();?>
		</div>
		<div class="columns small-12 medium-11 sp-banner-container">
			<h2>Mission</h2>
		</div>
		<div class="columns small-12 medium-11 text-content padding2rem">
			<?php the_field('content_2');?>
		</div>
		<div class="columns small-12 medium-11 sp-banner-container">
			<h2>What our customers are saying.</h2>
		</div>
		<div class="columns small-12 medium-11">
			
				<?php
					$tArgs = array('post_type' => 'testimonial',
									'post_status' => 'publish',
									'posts_per_page' => 3,
									'order' => 'DESC',
									'orderby' => 'menu_order');
					
					// The Query
					$the_query = new WP_Query( $tArgs );
					
					// The Loop
					if ( $the_query->have_posts() ) {
						echo '<div class="testimonial-slider">';
						while ( $the_query->have_posts() ) {
							$the_query->the_post(); ?>
							<div class="row f-row align-top align-justify slide">
								<div class="columns small-12 medium-6 text-center text">
									<?php the_content();?>
									<p class="orange"><?php the_title(); ?></p>
								</div>
								<div class="columns show-for-medium medium-6 text-center image">
									<img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full'); ?>" alt="">
								</div>
							</div>
						<?php }
						echo '</div>';
						/* Restore original Post Data */
						wp_reset_postdata();
					} else {
						// no posts found
					}
				?>
			</div>
	</div>
</div>

<?php get_footer(); ?>