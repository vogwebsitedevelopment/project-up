add_filter('iwj_get_dashboard_menus', function($menus, $position){
    $dashboard_url = iwj_get_page_permalink( 'dashboard' );
    unset($menus['new-job']);
    unset($menus['jobs']);
    $menus['custom-menu'] = array(
        'url' => add_query_arg(array('iwj_tab' => 'custom-menu'), $dashboard_url),
        'title' => '<i class="fa fa-star"></i>' . __('My Custom menu', 'iwjob')
    );;
    return $menus;
}, 10, 2);