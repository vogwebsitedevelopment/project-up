<div class="modal fade" id="iwj-modal-apply-<?php echo get_the_ID(); ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="iwj-application-form iwj-popup-form" action="<?php the_permalink(); ?>" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo __('Submit Bid','iwjob'); ?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                
                <div class="modal-body">
                <div class="row">
        <div class="columns small-12 medium-11 sp-banner-container">
            
            <h2>Submit a Bid </h2>
</div>
</div>

                
                    <?php
                    $user = IWJ_User::get_user();
                    $show_form = true;
                    if(iwj_option('verify_account') && $user && !$user->is_verified()){
                        $show_form = false;
                        ?>
                        <div class="iwj-alert-box">
                            <?php echo sprintf( __('You must verify your account before you can apply to a project. <a href="%s">verify now</a>','iwjob'), iwj_get_page_permalink('verify_account')); ?>
                        </div>
                        <?php
                    }elseif($user && iwj_option('allow_candidate_apply_job')){
                        $candidate = $user->get_candidate();
                        $profile_url = iwj_get_page_permalink('dashboard');
                        $profile_url = add_query_arg(array('iwj_tab'=>'profile'), $profile_url);

                        if($candidate) {
                            if(iwj_option('allow_candidate_apply_job') == '1' && !$candidate->is_active()) {
                                $show_form = false;
                                ?>
                                <div class="iwj-alert-box">
                                    <?php echo sprintf( __('Your profile must be activated before you can apply to project. <a href="%s">update profile</a>','iwjob'), $profile_url); ?>
                                </div>
                                <?php
                            }else{
                                $status = $candidate->get_status();
                                if(!in_array($status, array('pending', 'publish'))){
                                    $show_form = false;
                                    ?>
                                    <div class="iwj-alert-box">
                                        <?php echo sprintf( __('Your must submit a profile before applyying to a project. <a href="%s">submit now</a>','iwjob'), $profile_url); ?>
                                    </div>
                                    <?php
                                }
                            }
                        }
                    }

                    if($show_form){
                        $fields = $self->get_form_fields();
                        foreach ($fields as $field) {
                            $field = IWJMB_Field::call('normalize', $field);
                            $meta = IWJMB_Field::call($field, 'post_meta', 0, false);
                            IWJMB_Field::input($field, $meta);
                        }
                        ?>
                        <div class="iwj-respon-msg iwj-hide"></div>
                        <input type="hidden" name="job_id" value="<?php echo get_the_ID(); ?>">
                        <input type="hidden" name="action" value="iwj_submit_application">
                        <div class="iwj-btn-action">
                            <button type="button" class="iwj-btn" data-dismiss="modal"><?php echo __('Close', 'iwjob'); ?></button>
                            <div class="iwj-button-loader">
                                <button type="submit" class="iwj-btn iwj-btn-primary iwj-application-btn"><?php echo __('Submit', 'iwjob'); ?></button>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
    </div>
</div>