$(document).ready(function($){
	
	//Load Foundation!
	$(document).foundation();
	
	//Logout options reposition
	$('ul.dashboard-menu').each(function(){
		var logout = $('li').has('a i.fa-sign-out');
	$(this).find(logout).appendTo($(this));
	});
	
	//Add active class to dashboard menu
	var url = window.location.href;
	$('ul.dashboard-menu li').each(function(){
		var href = $('a', this).attr('href');
		if(href == url){
			$(this).addClass('active');
		}
	});

	//Registration bar slipper
	if($('div.top-bar-right ul.register-login').length){
		var width = $('div.top-bar-right ul.register-login li:last-of-type').width();
		var position = $('div.top-bar-right ul.register-login li:last-of-type').position();
		$('div.top-bar-right ul.register-login div#slipper').css('left', position.left).css('width', width);
		$('div.top-bar-right ul.register-login li').hover(function(){
				var position = $(this).position();
				var width = $(this).width();
				console.log(position.left);
				$('div.top-bar-right ul.register-login div#slipper').css('left', position.left).css('width', width);
		}, function(){
			var width = $('div.top-bar-right ul.register-login li:last-of-type').width();
			var position = $('div.top-bar-right ul.register-login li:last-of-type').position();
			$('div.top-bar-right ul.register-login div#slipper').css('left', position.left).css('width', width);
		}	);
	}
	
	$('span.select_cv').html('Upload Document');
	
	/*//Smooth internal link scrolling
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});*/
	
	//Make .square containers have a height equal to their width!
	$('.square').each(function(){
		var width = $(this).width();
		var height = $(this).height();
		if(width > height){
			$(this).css('height', width);}
		if(height > width){
			$(this).css('width', height);}
		});
	
	$(window).resize(function(){
		$('.square').each(function(){
			var squareNew = $(this).width();
			$(this).removeAttr('style').css('height', squareNew);
		});
	});
	
	//Make video iframes proper aspect ratio
	var vidWidth = $('.video-box iframe').width();
	
	var vidUnit = vidWidth/16;
	
	var vidHeight = vidUnit*9;
	
	$('.video-box iframe').css('height', vidHeight);
		
	
	//Testimonial Slider
	$('.testimonial-slider').slick({
		infinite: true,
		slidesToShow: 1,
		dots: true,
		arrows: true,
		prevArrow: '<div class="slick-prev"><i class="fa fa-arrow-circle-left"></i></div>',
		nextArrow: '<div class="slick-next"><i class="fa fa-arrow-circle-right"></i></div>'
	});
	
	//History Back Button!
	$('a.history-back').click(function(){
		window.history.back();
	});
	
});

