<?php get_header(); ?>
<?php $styleDir = get_stylesheet_directory_uri(); ?>

<div class="fullwidth" id="topper">
	<div class="row f-row align-center align-middle">
		<div class="columns small-12 medium-6 show-for-medium">
			<img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full') ?>" alt="ProjectUp logo">
		</div>
		<div class="columns small-12 medium-6">
			<h1><?php the_title();?></h1>
			<p><?php the_field('descriptor_line');?></p>
		</div>
	</div>
</div>
<div class="fullwidth">
	<div class="row align-center f-row">
		<div class="columns small-12 medium-11 sp-banner-container">
			<h2>Send us a Message</h2>
		</div>
		<div class="columns small-12 medium-11">
			<?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]');?>
		</div>
	</div>
</div>

<?php get_footer(); ?>