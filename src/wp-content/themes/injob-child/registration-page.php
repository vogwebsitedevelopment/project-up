<?php /* Template Name: Registration Page Template */ ?>
<?php get_header(); ?>
<?php $styleDir = get_stylesheet_directory_uri(); ?>

<div class="fullwidth">
	<div class="row align-center f-row">
		<div class="columns small-12 medium-11 sp-banner-container">
			<h2><?php the_title(); ?></h2>
		</div>
		<div class="columns small-12 medium-11 content" id="registration">
			<?php the_content();?>
		</div>
	</div>
</div>

<?php get_footer();?>