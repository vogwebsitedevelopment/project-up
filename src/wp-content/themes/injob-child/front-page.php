<?php get_header(); ?>
<?php $styleDir = get_stylesheet_directory_uri(); ?>


<div class="fullwidth" id="topper">
	<div class="row f-row align-center align-middle">
		<div class="columns small-12 medium-6 show-for-medium">
			<img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full') ?>" alt="ProjectUp logo">
			
		</div>
		<div class="columns small-12 medium-6">
			<h1><?php the_title();?></h1>
			<p><?php the_field('descriptor_line');?></p>			
			<a href="/company-registration" class="button text-center">Post your project!</a>
			
		
		</div>
		
		
	</div>
	<!--<h3 class="text-center launching" style="color:#fff;background-color:rgba(69,114,153,0.7);padding: 2px 25px;position: absolute;bottom: 30px;margin-left: auto;margin-right: auto;left: 0;right: 0;text-align: center;">Launching June 2018 <span class="regis"> Register Now</span></h3>-->
</div>
<div class="fullwidth" id="tabs">
	<div class="row align-center f-row">
		<div class="columns small-12 medium-11 sp-banner-container">
			<h2>How It Works</h2>
		</div>
		<div class="columns small-12 medium-11 tab-box">
			<div class="row f-row align-center tabs" data-tabs id="front-tabs" data-options="scroll_to_content: false">
				<div class="small-5 medium-3 tabs-title is-active"><a href="#personal-panel" data-tabs-target="#personal-panel">Personal</a></div>
				<div class="show-for-medium medium-3"></div>
				<div class="small-5 medium-3 tabs-title"><a href="#business-panel" data-tabs-target="#business-panel"  >Business</a></div>
			</div>
			<div class="tabs-content" data-tabs-content="front-tabs">
				<div class="row f-row tabs-panel is-active" id="personal-panel">
					<?php
					$i=1;
						// check if the repeater field has rows of data
						if( have_rows('personal_tab') ){
							// loop through the rows of data
							while ( have_rows('personal_tab') ) {
								the_row();
								$n=$i++;?>
								<div class="columns small-12 medium-4">
									<div class="number">
										<span><?php echo $n;?></span>
									</div>
									<div class="image">
										<img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('description');?>"> 
									</div>
									<div class="description">
										<p><?php the_sub_field('description');?></p>
									</div>
								</div>
								<?php
						}
						}else{
							// no rows found
						}
					?>
				</div>
				<div class="row f-row tabs-panel" id="business-panel">
					<?php
					$i=1;
						// check if the repeater field has rows of data
						if( have_rows('business_tab') ){
							// loop through the rows of data
							while ( have_rows('business_tab') ) {
								the_row();
								$n=$i++;?>
								<div class="columns small-12 medium-4">
									<div class="number">
										<span><?php echo $n;?></span>
									</div>
									<div class="image">
										<img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('description');?>"> 
									</div>
									<div class="description">
										<p><?php the_sub_field('description');?></p>
									</div>
								</div>
								<?php
						}
						}else{
							// no rows found
						}
					?>
				</div>
			</div>
		</div>
		<div class="columns small-12 medium-11" style="text-align:center;">
			<a class="button" href="/how-it-works">Click here to learn More</a>
		</div>
	</div>
</div>
<div class="fullwidth" id="slider">
	<div class="row f-row align-center">
		<div class="columns small-12 medium-11 sp-banner-container">
			<h2>What our customers are saying</h2>
		</div>
		<div class="columns small-12 medium-11">
			
				<?php
					$tArgs = array('post_type' => 'testimonial',
									'post_status' => 'publish',
									'posts_per_page' => 3,
									'order' => 'DESC',
									'orderby' => 'menu_order');
					
					// The Query
					$the_query = new WP_Query( $tArgs );
					
					// The Loop
					if ( $the_query->have_posts() ) {
						echo '<div class="testimonial-slider">';
						while ( $the_query->have_posts() ) {
							$the_query->the_post(); ?>
							<div class="row f-row align-top align-justify slide">
								<div class="columns small-12 medium-6 text-center text">
									<?php the_content();?>
									<p class="orange"><?php the_title(); ?></p>
								</div>
								<div class="columns show-for-medium medium-6 text-center image">
									<img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full'); ?>" alt="">
								</div>
							</div>
						<?php }
						echo '</div>';
						/* Restore original Post Data */
						wp_reset_postdata();
					} else {
						// no posts found
					}
				?>
			</div>
		</div>
</div>

<div class="fullwidth" id="works">
	<div class="row f-row align-center">
		<div class="columns small-12 medium-11 sp-banner-container">
			<h2>Categories</h2>
		</div>
		<div class="columns small-12 medium-11">
			<div class="row f-row align-center bg-blue">
				<div class="columns small-12 medium-expand">
					<img src="<?php echo $styleDir; ?>/images/icon_construction-thin.png" alt="Contruction">
					<p>Construction</p>
				</div>
				<div class="columns small-12 medium-expand">
					<img src="<?php echo $styleDir; ?>/images/icon_calculator-thin.png" alt="Professionals">
					<p>Professionals</p>
				</div>
				<div class="columns small-12 medium-expand">
					<img src="<?php echo $styleDir; ?>/images/icon_brush-thin.png" alt="Renovations">
					<p>Renovations</p>
				</div>
				<div class="columns small-12 medium-expand">
					<img src="<?php echo $styleDir; ?>/images/icon_design-thin.png" alt="Design">
					<p>Design</p>
				</div>
				<div class="columns small-12 medium-expand">
					<img src="<?php echo $styleDir; ?>/images/icon_truck-thin.png" alt="And more">
					<p class="">And more..</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer();?>