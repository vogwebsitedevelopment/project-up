<?php get_header(); ?>
<?php $styleDir = get_stylesheet_directory_uri(); ?>

<div class="fullwidth" id="topper">
	<div class="row f-row align-center align-middle" data-equalizer>
		<div class="columns small-12-medium-6 show-for-medium" data-equalizer-watch>
			<img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full') ?>" alt="ProjectUp logo">
		</div>
		<div class="columns small-12-medium-6" data-equalizer-watch>
			<h1><?php the_title();?></h1>
			<p><?php the_field('descriptor_line');?></p>
		</div>
	</div>
</div>
<div class="fullwidth">
	<div class="row align-center f-row">
		<div class="columns small-12 medium-11 sp-banner-container">
			<h2>Blog</h2>
		</div>
		<div class="columns small-12 medium-11">
			<?php the_content();?>
		</div>
	</div>
</div>

<?php get_footer(); ?>