<?php
/**
 * InJob-Child functions and definitions
 *
 * @package InJob
 */

add_action( 'after_setup_theme', 'inwave_child_theme_setup' );

function inwave_child_theme_setup() {
    load_child_theme_textdomain( 'injob', get_stylesheet_directory() . '/languages' );
}

// Menus

function register_menus() {
	register_nav_menus(
		array(
			'candidate_menu' => __('Candidate Menu'),
            'employer_menu' => __('Employer Menu')
		)
	);
}

add_action('init', 'register_menus');

//Update jQuery

function update_jquery(){
    wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js');
		wp_enqueue_script('jquery');
};
add_action('wp_enqueue_scripts', 'update_jquery');

//Add Stylesheets

function stylesheets() {
	wp_enqueue_style('foundation', get_stylesheet_directory_uri() . '/css/app.css', __FILE__);
	//wp_enqueue_style('fontAwesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css', __FILE__);
}

//Add Javascripts

function javascripts() {
	wp_enqueue_script('jQueryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js');
	wp_enqueue_script('FontAwesome', 'https://use.fontawesome.com/db185c3a8d.js');
	wp_enqueue_script('foundation', get_stylesheet_directory_uri() . '/js/foundation.min.js');
	wp_enqueue_script('slick', get_stylesheet_directory_uri() . '/js/slick.min.js');
	wp_enqueue_script('customFunctions', get_stylesheet_directory_uri() . '/js/customFunctions.js');
}

add_action ('wp_enqueue_scripts', 'stylesheets');
add_action ('wp_enqueue_scripts', 'javascripts');

//Theme Supports

add_theme_support( 'post-thumbnails' );
add_post_type_support( 'page', 'excerpt' );

//Page Slug Body Class
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

//Format Phone Numbers!

function phoneFormat($data){
	echo '('.substr($data, 0, 3).') '.substr($data, 3, 3).'-'.substr($data,6);
}

//Add extra user roles on registration
 
function add_secondary_role( $user_id ) {
 
    $user = get_user_by('id', $user_id);
    
    $user_meta=get_userdata($user_id);
    
    $user_roles=$user_meta->roles;
    
    if (in_array('subscriber', $user_roles)){
    
        $user->add_role('iwj_employer');
        
    }else/*if (in_array('s2member_level1', $user_roles))*/{
        
        $user->add_role('iwj_candidate');
        
    };
};

add_action( 'user_register', 'add_secondary_role', 10, 1 );


//Custom Menus!
add_filter('iwj_get_dashboard_menus', function($menus, $position){
   $dashboard_url = iwj_get_page_permalink( 'dashboard' );
   unset($menus['new-job']);
   unset($menus['jobs']);
   unset($menus['follows']);
   unset($menus['save-jobs']);
   unset($menus['alerts']);
   unset($menus['my-reviews']);
   unset($menus['applications']);
   unset($menus['save-resumes']);
   unset($menus['reviews']);
   unset($menus['orders']);
   unset($menus['submited-applications']);
   //$menus['custom-menu'] = array(
       //'url' => add_query_arg(array('iwj_tab' => 'custom-menu'), $dashboard_url),
       //'title' => '<i class="fa fa-star"></i>' . __('My Custom menu', 'iwjob')
   //);
   
   $user = wp_get_current_user();
   if($user){
       //add menu for employer
       if(in_array('iwj_employer', $user->roles)){
           $menus['employer-menu'] = array(
               'url' => add_query_arg(array('iwj_tab' => 'new-job'), $dashboard_url),
               'title' => '<i class="fa fa-plus"></i>' . __('New Project', 'iwjob')
           );
           $menus['employer-menu2'] = array(
               'url' => add_query_arg(array('iwj_tab' => 'jobs'), $dashboard_url),
               'title' => '<i class="fa fa-briefcase"></i>' . __('My Projects', 'iwjob')
           );
           $menus['employer-menu3'] = array(
               'url' => add_query_arg(array('iwj_tab' => 'applications'), $dashboard_url),
               'title' => '<i class="fa fa-newspaper-o"></i>' . __('Responses', 'iwjob')
           );
           $menus['employer-menu4'] = array(
               'url' => add_query_arg(array('iwj_tab' => 'view-resumes'), $dashboard_url),
               'title' => '<i class="fa fa-eye"></i>' . __('Viewed Contractors', 'iwjob')
           );
           $menus['employer-menu5'] = array(
               'url' => add_query_arg(array('iwj_tab' => 'reviews'), $dashboard_url),
               'title' => '<i class="fa fa-star"></i>' . __('Reviews', 'iwjob')
           );
      }
       //add menu for candidate
       if(in_array('iwj_candidate', $user->roles)){
           $menus['candidate-menu'] = array(
               'url' => add_query_arg(array('iwj_tab' => 'submited-applications'), $dashboard_url),
               'title' => '<i class="fa fa-newspaper-o"></i>' . __('Projects Quoted', 'iwjob')
            );
            $menus['candidate-menu-2'] = array(
               'url' => add_query_arg(array('iwj_tab' => 'follows'), $dashboard_url),
               'title' => '<i class="fa fa-briefcase"></i>' . __('Followed Clients', 'iwjob')
            );
             $menus['candidate-menu-3'] = array(
               'url' => add_query_arg(array('iwj_tab' => 'save-jobs'), $dashboard_url),
               'title' => '<i class="fa fa-heart"></i>' . __('Saved Projects', 'iwjob')
            );
              $menus['candidate-menu-4'] = array(
               'url' => add_query_arg(array('iwj_tab' => 'alerts'), $dashboard_url),
               'title' => '<i class="fa fa-envelope-o"></i>' . __('Alerts', 'iwjob')
            );
             $menus['candidate-menu-5'] = array(
               'url' => add_query_arg(array('iwj_tab' => 'my-reviews'), $dashboard_url),
               'title' => '<i class="fa fa-pencil"></i>' . __('My Reviews', 'iwjob')
            );
      }
   }
   return $menus;   
}, 10, 2);


//removes register link from login page
add_filter('register','no_register_link');
function no_register_link($url){
    return '';
}