<!--Menu desktop-->
<?php

$user = IWJ_User::get_user();

if( $user ){
    if( $user->is_candidate() ){
        $theme_menu = Inwave_Helper::getPostOption('for_candidates_menu');
        $location = 'candidate_menu';
    }
    elseif( $user->is_employer() ){
        $theme_menu = Inwave_Helper::getPostOption('for_employer_menu');
        $location = 'employer_menu';
    }
}else{
    $theme_menu = Inwave_Helper::getPostOption('guest_menu');
    $location = 'primary';
}

    
    wp_nav_menu(array(
        "container_class" => "iw-main-menu",
        'menu' => $theme_menu,
        'theme_location' => $location,
        "menu_class" => "iw-nav-menu  nav-menu nav navbar-nav",
        "walker" => new Walker_Nav_Menu(),
    ));

?>
