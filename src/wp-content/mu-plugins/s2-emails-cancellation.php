<?php
add_action('init', 's2_cancellation_notification');

function s2_cancellation_notification() {
    global $wpdb;

    if ( !defined('WP_PLUGIN_DIR') ) {
        define('WP_PLUGIN_DIR', dirname(__FILE__) .'/../plugins');
    }

    $s2emails_dir = WP_PLUGIN_DIR . '/s2emails';
    // check if the s2emails module exists, if not, stop here
    if ( !file_exists( $s2emails_dir )
        || !file_exists($s2emails_dir . '/s2emails.php') ) {
        return false;
    }

    $log_path = $s2emails_dir .'/logs/';
    $_time = date('Y-m-d H:i:s');

    if ( !isset($_GET['s2_cancellation_notification'])
        || empty($_GET['s2_cancellation_notification'])
        || $_GET['s2_cancellation_notification'] != 'yes'
        || ( empty((int)$_GET['user_id']) || !is_numeric($_GET['user_id']) ) ) {
        @file_put_contents($log_path .'cancellation-'. date('Y-m-d') .'.log', $_time ."\n". 'call not allowed!' ."\n\n", FILE_APPEND);
        return;
    }

    require_once( $s2emails_dir .'/s2emails.php' );
    $user_id = (int)$_GET['user_id'];

    // create pending mail to send after cancellation
    $s2emails_repo = new S2emailRepository();
    $s2emails_repo->persistPendingMail($user_id, 6, 'after_cancel');

    @file_put_contents($log_path .'cancellation-'. date('Y-m-d') .'.log', $_time ."\n". print_r(['after_cancel is created with user: '. $user_id], true) ."\n\n", FILE_APPEND);
}