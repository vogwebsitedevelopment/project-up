<?php
/* Smarty version 3.1.30, created on 2018-05-10 20:08:53
  from "3833b4c0b44f1ca740e5cfcbb6ad8b0246c93a50" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5af4a6d59e5013_80238057',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5af4a6d59e5013_80238057 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html <?php echo $_smarty_tpl->tpl_vars['language_attributes']->value;?>
 >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->tpl_vars['charset']->value;?>
" />
		<title><?php echo $_smarty_tpl->tpl_vars['site_title']->value;?>
</title>
	</head>
	<body <?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rightmargin<?php } else { ?>leftmargin<?php }?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<div id="wrapper" dir="<?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rtl<?php } else { ?>ltr<?php }?>">
		    <table id="template_container">
                <tr>
                    <td id="template_top_header">
                        <!-- custom you logo here -->
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="template_table">
                            <thead id="template_header">
                                <tr>
                                    <td align="center" valign="top">
                                        <h1>New Registration</h1>
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="template_body">
                                <!-- Content -->
                                <tr>
                                    <td valign="top" id="body_content">
                                        <div id="body_content_inner">
											Hi Admin,
A new user has registered account on <?php echo $_smarty_tpl->tpl_vars['site_title']->value;?>


following account infomation:
- Display Name: <?php echo $_smarty_tpl->tpl_vars['display_name']->value;?>

- Email: <?php echo $_smarty_tpl->tpl_vars['email']->value;?>

- Role: <?php echo $_smarty_tpl->tpl_vars['role']->value;?>


You can manager <a href="<?php echo $_smarty_tpl->tpl_vars['edit_url']->value;?>
" target="_blank">here</a>
										 </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
				<tr>
					<td id="template_footer">
						<center>Copyright © 2017 InwaveThemes Inc., All rights reserved.</center>
					</td>
				</tr>
            </table>
		</div>
	</body>
</html><?php }
}
