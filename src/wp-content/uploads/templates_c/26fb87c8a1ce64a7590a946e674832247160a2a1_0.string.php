<?php
/* Smarty version 3.1.30, created on 2018-05-10 20:08:52
  from "26fb87c8a1ce64a7590a946e674832247160a2a1" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5af4a6d48004e6_63240500',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5af4a6d48004e6_63240500 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html <?php echo $_smarty_tpl->tpl_vars['language_attributes']->value;?>
 >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->tpl_vars['charset']->value;?>
" />
		<title><?php echo $_smarty_tpl->tpl_vars['site_title']->value;?>
</title>
	</head>
	<body <?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rightmargin<?php } else { ?>leftmargin<?php }?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<div id="wrapper" dir="<?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rtl<?php } else { ?>ltr<?php }?>">
		    <table id="template_container">
                <tr>
                    <td id="template_top_header">
                        <!-- custom you logo here -->
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="template_table">
                            <thead id="template_header">
                                <tr>
                                    <td align="center" valign="top">
                                        <h1>New Registration</h1>
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="template_body">
                                <!-- Content -->
                                <tr>
                                    <td valign="top" id="body_content">
                                        <div id="body_content_inner">
											Congratulations! Your account has been registered at <?php echo $_smarty_tpl->tpl_vars['site_title']->value;?>
 
Your username : <?php echo $_smarty_tpl->tpl_vars['user_login']->value;?>

<?php if ($_smarty_tpl->tpl_vars['auto_generate_password']->value == true) {?>
Your password has been automatically generated : <?php echo $_smarty_tpl->tpl_vars['user_password']->value;?>

<?php } else { ?>
Your password : <?php echo $_smarty_tpl->tpl_vars['user_password']->value;?>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['verify_account']->value == true) {?>
Please click <a href="<?php echo $_smarty_tpl->tpl_vars['activation_url']->value;?>
" target="_blank">here</a> to verify your account
<?php } else { ?>
You can manage your account <a href="<?php echo $_smarty_tpl->tpl_vars['dashboard_url']->value;?>
">here</a>
<?php }?>

Thank you for choosing InJob!
InJob Team.
										 </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
				<tr>
					<td id="template_footer">
						<center>Copyright © 2017 InwaveThemes Inc., All rights reserved.</center>
					</td>
				</tr>
            </table>
		</div>
	</body>
</html><?php }
}
