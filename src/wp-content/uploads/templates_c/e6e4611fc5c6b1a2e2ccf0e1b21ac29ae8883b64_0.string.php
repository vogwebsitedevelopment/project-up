<?php
/* Smarty version 3.1.30, created on 2018-05-10 20:17:10
  from "e6e4611fc5c6b1a2e2ccf0e1b21ac29ae8883b64" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5af4a8c6876af2_32552108',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5af4a8c6876af2_32552108 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html <?php echo $_smarty_tpl->tpl_vars['language_attributes']->value;?>
 >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->tpl_vars['charset']->value;?>
" />
		<title><?php echo $_smarty_tpl->tpl_vars['site_title']->value;?>
</title>
	</head>
	<body <?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rightmargin<?php } else { ?>leftmargin<?php }?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<div id="wrapper" dir="<?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rtl<?php } else { ?>ltr<?php }?>">
		    <table id="template_container">
                <tr>
                    <td id="template_top_header">
                        <!-- custom you logo here -->
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="template_table">
                            <thead id="template_header">
                                <tr>
                                    <td align="center" valign="top">
                                        <h1>Review Job</h1>
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="template_body">
                                <!-- Content -->
                                <tr>
                                    <td valign="top" id="body_content">
                                        <div id="body_content_inner">
											<?php if ($_smarty_tpl->tpl_vars['is_child_job']->value == true) {?>
Hi Admin <?php echo $_smarty_tpl->tpl_vars['author_name']->value;?>
 has been submited an update of <a href="<?php echo $_smarty_tpl->tpl_vars['parent_job']->value->permalink();?>
"><?php echo $_smarty_tpl->tpl_vars['parent_job']->value->get_title();?>
</a> you should go to browse it
<?php } else { ?>
Hi Admin <?php echo $_smarty_tpl->tpl_vars['author_name']->value;?>
 has been submited job <a href="<?php echo $_smarty_tpl->tpl_vars['job']->value->permalink();?>
"><?php echo $_smarty_tpl->tpl_vars['job']->value->get_title();?>
</a> you should go to browse it
<?php }?>
You can review <a href="<?php echo $_smarty_tpl->tpl_vars['job']->value->admin_link();?>
">here</a>
										 </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
				<tr>
					<td id="template_footer">
						<center>Copyright © 2017 InwaveThemes Inc., All rights reserved.</center>
					</td>
				</tr>
            </table>
		</div>
	</body>
</html><?php }
}
