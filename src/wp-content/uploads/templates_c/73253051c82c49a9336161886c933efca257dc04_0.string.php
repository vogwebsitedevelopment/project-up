<?php
/* Smarty version 3.1.30, created on 2018-05-10 20:17:03
  from "73253051c82c49a9336161886c933efca257dc04" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5af4a8bf8a8d58_01784388',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5af4a8bf8a8d58_01784388 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html <?php echo $_smarty_tpl->tpl_vars['language_attributes']->value;?>
 >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->tpl_vars['charset']->value;?>
" />
		<title><?php echo $_smarty_tpl->tpl_vars['site_title']->value;?>
</title>
	</head>
	<body <?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rightmargin<?php } else { ?>leftmargin<?php }?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<div id="wrapper" dir="<?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rtl<?php } else { ?>ltr<?php }?>">
		    <table id="template_container">
                <tr>
                    <td id="template_top_header">
                        <!-- custom you logo here -->
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="template_table">
                            <thead id="template_header">
                                <tr>
                                    <td align="center" valign="top">
                                        <h1>Thank you for ordering!</h1>
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="template_body">
                                <!-- Content -->
                                <tr>
                                    <td valign="top" id="body_content">
                                        <div id="body_content_inner">
											Hi <?php echo $_smarty_tpl->tpl_vars['author_name']->value;?>
,
You have create an order #<?php echo $_smarty_tpl->tpl_vars['order_number']->value;?>
 on <?php echo $_smarty_tpl->tpl_vars['site_title']->value;?>
 please see following details:

=================================================
<?php echo $_smarty_tpl->tpl_vars['order_description']->value;?>

Order Number : <?php echo $_smarty_tpl->tpl_vars['order_number']->value;?>

Created : <?php echo $_smarty_tpl->tpl_vars['order_date']->value;?>

Total Price : <?php echo iwj_system_price($_smarty_tpl->tpl_vars['order']->value->get_price(),$_smarty_tpl->tpl_vars['order']->value->get_currency());?>

Order Status : <?php echo $_smarty_tpl->tpl_vars['order']->value->get_status_title($_smarty_tpl->tpl_vars['order']->value->get_status());?>

Payment Method : <?php echo $_smarty_tpl->tpl_vars['order']->value->get_payment_method_title();?>

=================================================

Thank you!
InJob team
										 </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
				<tr>
					<td id="template_footer">
						<center>Copyright © 2017 InwaveThemes Inc., All rights reserved.</center>
					</td>
				</tr>
            </table>
		</div>
	</body>
</html><?php }
}
