<?php
/* Smarty version 3.1.30, created on 2018-06-06 18:52:58
  from "1e64ab1779473c8dde7d7b2f68c2fb14737c468f" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b182d8a3319a1_01099323',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b182d8a3319a1_01099323 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html <?php echo $_smarty_tpl->tpl_vars['language_attributes']->value;?>
 >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->tpl_vars['charset']->value;?>
" />
		<title><?php echo $_smarty_tpl->tpl_vars['site_title']->value;?>
</title>
	</head>
	<body <?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rightmargin<?php } else { ?>leftmargin<?php }?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<div id="wrapper" dir="<?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rtl<?php } else { ?>ltr<?php }?>">
		    <table id="template_container">
                <tr>
                    <td id="template_top_header">
                        <!-- custom you logo here -->
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="template_table">
                            <thead id="template_header">
                                <tr>
                                    <td align="center" valign="top">
                                        <h1>Thank you for your application</h1>
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="template_body">
                                <!-- Content -->
                                <tr>
                                    <td valign="top" id="body_content">
                                        <div id="body_content_inner">
											Dear <?php echo $_smarty_tpl->tpl_vars['candidate_name']->value;?>
,
Your application has been sent to <?php echo $_smarty_tpl->tpl_vars['employer_name']->value;?>
. The employer will consider and contact to you as soon as possible if your abilities match their requirement.

Your detailed application:

=================================================
Fullname :  <?php echo $_smarty_tpl->tpl_vars['application']->value->get_full_name();?>

Email : <?php echo $_smarty_tpl->tpl_vars['application']->value->get_email();?>

<?php $_smarty_tpl->_assignInScope('cv', $_smarty_tpl->tpl_vars['application']->value->get_cv());
?>
Candidate CV :  <?php if (isset($_smarty_tpl->tpl_vars['cv']->value)) {?><a href="<?php echo $_smarty_tpl->tpl_vars['cv']->value['url'];?>
">Download full CV</a><?php }?>
Candidate Message : 
<?php echo $_smarty_tpl->tpl_vars['application']->value->get_message();?>

=================================================

Thank you!
Project Up Team.
										 </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
				<tr>
					<td id="template_footer">
						<center>Copyright © 2018 Project Up Inc., All rights reserved.</center>
					</td>
				</tr>
            </table>
		</div>
	</body>
</html><?php }
}
