<?php
/* Smarty version 3.1.30, created on 2018-06-06 04:13:11
  from "8da6cd874005fed1bdcad75c8a6e4cfd92975ad2" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b175f57c40ee8_02041346',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b175f57c40ee8_02041346 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html <?php echo $_smarty_tpl->tpl_vars['language_attributes']->value;?>
 >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->tpl_vars['charset']->value;?>
" />
		<title><?php echo $_smarty_tpl->tpl_vars['site_title']->value;?>
</title>
	</head>
	<body <?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rightmargin<?php } else { ?>leftmargin<?php }?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<div id="wrapper" dir="<?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rtl<?php } else { ?>ltr<?php }?>">
		    <table id="template_container">
                <tr>
                    <td id="template_top_header">
                        <!-- custom you logo here -->
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="template_table">
                            <thead id="template_header">
                                <tr>
                                    <td align="center" valign="top">
                                        <h1>New Job From <?php echo $_smarty_tpl->tpl_vars['author_name']->value;?>
</h1>
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="template_body">
                                <!-- Content -->
                                <tr>
                                    <td valign="top" id="body_content">
                                        <div id="body_content_inner">
											Dear <?php echo $_smarty_tpl->tpl_vars['follower_name']->value;?>
, <?php echo $_smarty_tpl->tpl_vars['author_name']->value;?>
 submited a job <a href="<?php echo $_smarty_tpl->tpl_vars['job']->value->permalink();?>
">job</a> on <?php echo $_smarty_tpl->tpl_vars['site_title']->value;?>


<div class="job-item">
    <img src="<?php echo get_avatar_url($_smarty_tpl->tpl_vars['author']->value->get_id());?>
" alt="<?php echo $_smarty_tpl->tpl_vars['job']->value->get_title();?>
">
    <h3><?php echo $_smarty_tpl->tpl_vars['job']->value->get_title();?>
</h3>
    <ul class="job-meta">
        <li class="author">
            <span>Author : </span>
            <span><a href="<?php echo $_smarty_tpl->tpl_vars['author']->value->permalink();?>
"><?php echo $_smarty_tpl->tpl_vars['author']->value->get_display_name();?>
</a></span>
        </li>
        <li class="salary">
            <span>Sallary : </span>
            <span><?php echo $_smarty_tpl->tpl_vars['job']->value->get_salary();?>
</span>
        </li>
        <li class="address">
            <span>Address : </span>
            <span><?php echo $_smarty_tpl->tpl_vars['job']->value->get_address();?>
</span>
        </li>
    </ul>
</div>

										 </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
				<tr>
					<td id="template_footer">
						<center>Copyright © 2018 Project Up Inc., All rights reserved.</center>
					</td>
				</tr>
            </table>
		</div>
	</body>
</html><?php }
}
