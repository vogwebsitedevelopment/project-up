<?php
/* Smarty version 3.1.30, created on 2018-06-12 22:32:32
  from "572efb8e4729ab7a6bfb7ef52c2afa5b1e2d0800" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b204a00834c58_55743698',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b204a00834c58_55743698 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html <?php echo $_smarty_tpl->tpl_vars['language_attributes']->value;?>
 >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->tpl_vars['charset']->value;?>
" />
		<title><?php echo $_smarty_tpl->tpl_vars['site_title']->value;?>
</title>
	</head>
	<body <?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rightmargin<?php } else { ?>leftmargin<?php }?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<div id="wrapper" dir="<?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rtl<?php } else { ?>ltr<?php }?>">
		    <table id="template_container">
                <tr>
                    <td id="template_top_header">
                        <!-- custom you logo here -->
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="template_table">
                            <thead id="template_header">
                                <tr>
                                    <td align="center" valign="top">
                                        <h1>You have a new review </h1>
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="template_body">
                                <!-- Content -->
                                <tr>
                                    <td valign="top" id="body_content">
                                        <div id="body_content_inner">
											Dear <?php echo $_smarty_tpl->tpl_vars['employer_name']->value;?>

<br>
A Service Provided with the username  
<?php echo $_smarty_tpl->tpl_vars['candidate_name']->value;?>
 has written a review for your Business . Below are the details.
<br><br>
Review Title: <?php echo $_smarty_tpl->tpl_vars['review_title']->value;?>
 <br>
Total Rating: <?php echo $_smarty_tpl->tpl_vars['rating']->value;?>
 <br>
Review Content: <?php echo $_smarty_tpl->tpl_vars['review_content']->value;?>
 <br>

You can view the review <a href="<?php echo $_smarty_tpl->tpl_vars['review_url']->value;?>
" target="_blank">here</a>
<br><br>
Thank you for choosing Project Up!
<br>Project Up Team.
										 </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
				<tr>
					<td id="template_footer">
						<center>Copyright © 2018 Project Up Inc., All rights reserved.</center>
					</td>
				</tr>
            </table>
		</div>
	</body>
</html><?php }
}
