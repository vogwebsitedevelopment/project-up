<?php
/* Smarty version 3.1.30, created on 2018-06-14 17:49:08
  from "9e6930ebdd5a29eb97795e5aa4ef8ecfa0105061" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b22aa947f4b85_15460725',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b22aa947f4b85_15460725 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html <?php echo $_smarty_tpl->tpl_vars['language_attributes']->value;?>
 >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_smarty_tpl->tpl_vars['charset']->value;?>
" />
		<title><?php echo $_smarty_tpl->tpl_vars['site_title']->value;?>
</title>
	</head>
	<body <?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rightmargin<?php } else { ?>leftmargin<?php }?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<div id="wrapper" dir="<?php if ($_smarty_tpl->tpl_vars['is_rtl']->value == 1) {?>rtl<?php } else { ?>ltr<?php }?>">
		    <table id="template_container">
                <tr>
                    <td id="template_top_header">
                        <!-- custom you logo here -->
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="template_table">
                            <thead id="template_header">
                                <tr>
                                    <td align="center" valign="top">
                                        <h1><?php echo $_smarty_tpl->tpl_vars['total_jobs']->value;?>
 new <?php echo $_smarty_tpl->tpl_vars['position']->value;?>
 project(s) for:</h1>
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="template_body">
                                <!-- Content -->
                                <tr>
                                    <td valign="top" id="body_content">
                                        <div id="body_content_inner">
											Hi <?php echo $_smarty_tpl->tpl_vars['display_name']->value;?>
,  <br>These are new project(s) that are available :

<div class="job-list job-list-email">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['jobs']->value, 'job');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['job']->value) {
?>
    <?php $_smarty_tpl->_assignInScope('author', $_smarty_tpl->tpl_vars['job']->value->get_author());
?>
   <div class="job-item">
            <div class="image">
                <img src="<?php echo get_avatar_url($_smarty_tpl->tpl_vars['author']->value->get_id());?>
" alt="<?php echo $_smarty_tpl->tpl_vars['job']->value->get_title();?>
">
            </div>
            <div class="info-wrap">
                <h3><?php echo $_smarty_tpl->tpl_vars['job']->value->get_title();?>
</h3>
                <ul class="job-meta">
                    <li class="author">
                        <span>Author</span>:<span><a href="<?php echo $_smarty_tpl->tpl_vars['author']->value->permalink();?>
"><?php echo $_smarty_tpl->tpl_vars['author']->value->get_display_name();?>
</a></span>
                    </li>
                    <li class="salary">
                        <span>Budget:</span>:<span><?php echo $_smarty_tpl->tpl_vars['job']->value->get_salary();?>
</span>
                    </li>
                    <li class="address">
                       <span>Address</span>:<span><?php echo $_smarty_tpl->tpl_vars['job']->value->get_full_address();?>
</span>
                    </li>
                </ul>
            </div>
        </div>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</div>

Thank you for choosing Project Up! <br>
Project Up Team. <br>

Don't want to receive new project alerts anymore? <a href="<?php echo $_smarty_tpl->tpl_vars['unsubscribe_link']->value;?>
">Unsubscribe</a>.
										 </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
				<tr>
					<td id="template_footer">
						<center>Copyright © 2018 Project Up Inc., All rights reserved.</center>
					</td>
				</tr>
            </table>
		</div>
	</body>
</html><?php }
}
