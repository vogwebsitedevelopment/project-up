<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('WP_HOME','http://project-up');
define('WP_SITEURL','http://project-up');
 
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_project_up');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|~l:iI+cm&[Ol-&aWZ~.MTMn 9++5]BZBSC-Z@2d#Rr#[|xT`dx)CB;>*MhA;y8r');
define('SECURE_AUTH_KEY',  'rT=b1A4%LNpE@U=8[i[+:b4G,|IYh8ejNnhVcc>6A_=>TcG]e-O(D9{H+ihp|YLs');
define('LOGGED_IN_KEY',    '4te]JMC>.g[]H+ce -*,[*R8Jc9-^}6+!y >p_<*.Q-+8pm*ZvEtWrNcZA!T;E5=');
define('NONCE_KEY',        '|I:@s.G+e?fC~tno)hn<l-cT5Xb_ k4Sks3tt.NSU8b-]tO7w_v[zVtA#ec)|b49');
define('AUTH_SALT',        'iyc|%Na[&<`8<5T]Y|Lu5U2(z<6XNb8/$kA{GK4A&:r2I^+20:hk_{4sY0O`e-? ');
define('SECURE_AUTH_SALT', '3Zh[^AE]vcO+,7-ViKy@!8eU}WPZU+K7]rXL%W)LU}51$f[ekpW6Fw(4-X1b$F/1');
define('LOGGED_IN_SALT',   'jA=3YfUL7{7Un-c ](>3z gm+2KL*MuxOJ^-O-[rD(Mfx#[:D,lsRK`?NC:3Zey`');
define('NONCE_SALT',       'u/V)- Q{KQV! oGB}m|q/z1R]>#;}Ofy+tX@E?-ZTu?%.sXO+wb4x9=5YLa<.<J_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
